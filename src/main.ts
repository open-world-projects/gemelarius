import { LOCALE_ID, enableProdMode } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouter } from '@angular/router';
import en from '@angular/common/locales/en';

import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';

import { APP_ROUTES, AppComponent } from '@app';
import { environment } from '@env';
import { STATE_SAVER_TOKEN } from '@models/common/interfaces/StateSaver';
import { StateSaverLocalStorage } from '@models/common/StateSaverLocalStorage';
import { ROUTE_APP_TOKEN, RouteApp } from '@models/common/RouteApp';
import { provideHttpClient } from '@angular/common/http';

if (environment.production) {
    enableProdMode();
}

registerLocaleData(en);

bootstrapApplication(AppComponent, {
    providers: [
        provideHttpClient(),
        provideAnimations(),
        provideRouter(APP_ROUTES),
        { provide: NZ_I18N, useValue: en_US },
        { provide: LOCALE_ID, useValue: 'ru' },
        { provide: STATE_SAVER_TOKEN, useValue: new StateSaverLocalStorage() },
        { provide: ROUTE_APP_TOKEN, useValue: RouteApp },
    ],
}).catch(console.error);
