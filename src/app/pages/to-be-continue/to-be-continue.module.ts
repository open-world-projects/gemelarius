import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToBeContinueComponent } from './to-be-continue.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [ToBeContinueComponent],
    imports: [CommonModule, RouterModule.forChild([{ path: '', component: ToBeContinueComponent }])],
})
export class ToBeContinueModule {}
