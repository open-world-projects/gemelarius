import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { EmersionAnimation } from '@models/animation';

@Component({
    selector: 'app-to-be-continue',
    templateUrl: './to-be-continue.component.html',
    styleUrls: ['./to-be-continue.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [EmersionAnimation],
})
export class ToBeContinueComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}
}
