import {
    ChangeDetectionStrategy,
    Component,
    DestroyRef,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    effect,
    inject,
    signal,
    viewChild,
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { filter, finalize, fromEvent, map, switchMap, takeUntil, tap } from 'rxjs';

export type FieldPoint = { x: number; y: number };

@Component({
    selector: 'app-point-field',
    templateUrl: './point-field.component.html',
    styleUrl: './point-field.component.less',
    standalone: true,
    imports: [],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PointFieldComponent implements OnInit {
    @Input() size = 5;
    @Input() lines: [FieldPoint, FieldPoint][] = [];

    @Output() success = new EventEmitter();

    protected matrix: boolean[][] = [];

    protected prevCoords = signal<FieldPoint | null>(null);
    protected mouseCoords = signal<FieldPoint | null>(null);
    protected readonly map = new Map<string, boolean>();

    private readonly svg = viewChild<ElementRef>('svg');
    private readonly destroyRef = inject(DestroyRef);

    constructor() {
        effect(() => {
            const svg = this.svg();
            if (!svg) {
                return;
            }
            const down = fromEvent<MouseEvent>(svg.nativeElement, 'mousedown');
            const up = fromEvent<MouseEvent>(window.document, 'mouseup');
            const move = fromEvent<MouseEvent>(svg.nativeElement, 'mousemove');
            down.pipe(
                map((e) => e.target as HTMLElement),
                filter((e) => e.tagName === 'circle'),
                tap((e) => {
                    this.prevCoords.set({ x: Number(e.dataset['x']), y: Number(e.dataset['y']) });
                    for (const [key, _] of this.map.entries()) {
                        this.map.set(key, false);
                    }
                }),
                switchMap((prev) =>
                    move.pipe(
                        tap((curr) => {
                            this.mouseCoords.set({ x: curr.offsetX, y: curr.offsetY });

                            const target = curr.target as HTMLElement;
                            if (target.tagName === 'circle') {
                                const path = `${prev.dataset['y']}-${prev.dataset['x']}:${target.dataset['y']}-${target.dataset['x']}`;
                                const rePath = `${target.dataset['y']}-${target.dataset['x']}:${prev.dataset['y']}-${prev.dataset['x']}`;
                                if (this.map.get(path) === false || this.map.get(rePath) === false) {
                                    this.map.set(path, true);
                                    this.map.set(rePath, true);
                                    this.prevCoords.set({
                                        x: Number(target.dataset['x']),
                                        y: Number(target.dataset['y']),
                                    });
                                    prev = target;
                                    this.check();
                                }
                            }
                        }),
                        takeUntil(up),
                        finalize(() => this.mouseCoords.set(null))
                    )
                ),
                takeUntilDestroyed(this.destroyRef)
            ).subscribe();
        });
    }

    ngOnInit(): void {
        const matrix = [];

        const unique = new Set(
            this.lines.reduce((a, b) => (a.push(...b), a), [] as FieldPoint[]).map((e) => `${e.y}-${e.x}`)
        );

        for (let i = 0; i < this.size; i++) {
            const row: any[] = [];
            for (let j = 0; j < this.size; j++) {
                row.push(unique.has(`${i}-${j}`));
            }
            matrix.push(row);
        }
        this.matrix = matrix;

        this.lines.forEach((line) => {
            this.map.set(`${line[0].y}-${line[0].x}:${line[1].y}-${line[1].x}`, false);
            this.map.set(`${line[1].y}-${line[1].x}:${line[0].y}-${line[0].x}`, false);
        });
    }

    private check() {
        const size = this.map.size;
        let count = 0;
        this.map.forEach((item) => (item ? count++ : 0));
        if (size === count) {
            this.success.emit();
        }
    }
}
