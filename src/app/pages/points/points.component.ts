import { ChangeDetectionStrategy, Component, DestroyRef, inject, signal } from '@angular/core';
import { EmersionAnimation } from '@models/animation';
import { ProgressService } from '@services/progress.service';
import { FieldPoint, PointFieldComponent } from './components/point-field/point-field.component';
import { timer } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
    selector: 'app-points',
    standalone: true,
    imports: [PointFieldComponent],
    templateUrl: './points.component.html',
    styleUrl: './points.component.less',
    animations: [EmersionAnimation],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PointsComponent {
    private readonly progress = inject(ProgressService);
    private readonly destroyRef = inject(DestroyRef);
    end = false;
    done = signal(false);

    lines: [FieldPoint, FieldPoint][] = [
        [
            { x: 2, y: 1 },
            { x: 1, y: 2 },
        ],
        [
            { x: 2, y: 1 },
            { x: 3, y: 2 },
        ],
        [
            { x: 2, y: 1 },
            { x: 3, y: 2 },
        ],
        [
            { x: 1, y: 2 },
            { x: 2, y: 2 },
        ],
        [
            { x: 1, y: 2 },
            { x: 2, y: 3 },
        ],
        [
            { x: 1, y: 2 },
            { x: 1, y: 3 },
        ],
        [
            { x: 3, y: 2 },
            { x: 2, y: 2 },
        ],
        [
            { x: 3, y: 2 },
            { x: 2, y: 3 },
        ],
        [
            { x: 3, y: 2 },
            { x: 3, y: 3 },
        ],
        [
            { x: 3, y: 4 },
            { x: 3, y: 3 },
        ],
        [
            { x: 3, y: 4 },
            { x: 2, y: 3 },
        ],
        [
            { x: 3, y: 4 },
            { x: 2, y: 4 },
        ],
        [
            { x: 1, y: 4 },
            { x: 1, y: 3 },
        ],
        [
            { x: 1, y: 4 },
            { x: 2, y: 3 },
        ],
        [
            { x: 1, y: 4 },
            { x: 2, y: 4 },
        ],
    ];

    success() {
        this.end = true;
        timer(1500)
            .pipe(takeUntilDestroyed(this.destroyRef))
            .subscribe(() => this.progress.success());
    }
}
