import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { takeUntil, timer } from 'rxjs';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';
import { Utils } from '@Utils';

@Component({
    selector: 'app-number-tree',
    templateUrl: './number-tree.component.html',
    styleUrls: ['./number-tree.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
})
export class NumberTreeComponent implements OnInit {
    readonly EXPECTED_NUMBER = Utils.RandomValueFromRange(1, 20);

    constructor(private _destroy: DestroyService, private _progress: ProgressService) {}

    ngOnInit(): void {}

    complete() {
        timer(1000).pipe(takeUntil(this._destroy)).subscribe(this._progress.success.bind(this._progress));
    }
}
