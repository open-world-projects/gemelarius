import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NumberTreeComponent } from './number-tree.component';
import { TreeNumberSearchModule } from '@components/tree-number-search/tree-number-search.module';

@NgModule({
    declarations: [NumberTreeComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([{ path: '', component: NumberTreeComponent }]),
        TreeNumberSearchModule,
    ],
})
export class NumberTreeModule {}
