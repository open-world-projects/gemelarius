import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnInit,
    ViewChild,
} from '@angular/core';
import { BehaviorSubject, takeUntil, timer } from 'rxjs';
import { Utils } from '@Utils';
import { BubbleListType } from './BubbleListType';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BubbleAnimation, EmersionAnimation } from '@models/animation';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-bubble-number',
    templateUrl: './bubble-number.component.html',
    styleUrls: ['./bubble-number.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [BubbleAnimation, EmersionAnimation],
    providers: [DestroyService],
})
export class BubbleNumberComponent implements OnInit, AfterViewInit {
    readonly COUNT_BUBBLES: number = 50;

    list$: BehaviorSubject<BubbleListType[]> = new BehaviorSubject<BubbleListType[]>([]);

    @ViewChild('field') field!: ElementRef<HTMLElement>;

    success$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(
        private _ref: ChangeDetectorRef,
        private _msg: NzMessageService,
        private _destroy: DestroyService,
        private _progress: ProgressService
    ) {}

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        const width = this.field?.nativeElement?.offsetWidth ?? 0;
        const height = this.field?.nativeElement?.offsetHeight ?? 0;
        if (!this.field) {
            this._msg.error('Error initialiaze field bubble!');
            return;
        }

        const list: BubbleListType[] = Array.from({ length: this.COUNT_BUBBLES }).map((e, i) => ({
            ordinal: i + 1,
            initX: Utils.RandomValueFromRange(0, width - 150),
            initY: Utils.RandomValueFromRange(0, height - 150),
        }));

        this.list$.next(list);

        // TODO: crutch
        this._ref.detectChanges();
    }

    clickedBubble(index: number) {
        const list = this.list$.getValue();
        const last = list[list.length - 1];
        if (index !== last.ordinal) {
            this._msg.warning('Please click on the bubbles in descending order');
        } else {
            list.splice(list.length - 1, 1);
        }
        !list.length && this.complete();
    }

    complete() {
        this.success$.next(true);
        timer(500).pipe(takeUntil(this._destroy)).subscribe(this._progress.success.bind(this._progress));
    }
}
