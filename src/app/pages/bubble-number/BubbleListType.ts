export interface BubbleListType {
    ordinal: number;
    initX: number;
    initY: number;
}
