import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BubbleNumberComponent } from './bubble-number.component';
import { RouterModule } from '@angular/router';
import { BubbleModule } from '@components/bubble/bubble.module';

@NgModule({
    declarations: [BubbleNumberComponent],
    imports: [CommonModule, RouterModule.forChild([{ path: '', component: BubbleNumberComponent }]), BubbleModule],
})
export class BubbleNumberModule {}
