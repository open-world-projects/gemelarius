import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-cycle-number',
    templateUrl: './cycle-number.component.html',
    styleUrls: ['./cycle-number.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CycleNumberComponent implements OnInit {
    constructor(private readonly _progress: ProgressService) {}

    ngOnInit(): void {}

    success() {
        this._progress.success();
    }
}
