import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CycleNumberComponent } from './cycle-number.component';
import { RouterModule } from '@angular/router';
import { CycleNumberSceneModule } from '@components/cycle-number/cycle-number-scene/cycle-number-scene.module';

@NgModule({
    declarations: [CycleNumberComponent],
    imports: [
        CommonModule,
        CycleNumberSceneModule,
        RouterModule.forChild([{ path: '', component: CycleNumberComponent }]),
    ],
})
export class CycleNumberModule {}
