import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-image-projection',
    templateUrl: './image-projection.component.html',
    styleUrls: ['./image-projection.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageProjectionComponent implements OnInit {
    readonly EXPECTED_WORD = 'never';

    constructor(private _progress: ProgressService) {}

    ngOnInit(): void {}

    success() {
        this._progress.success();
    }
}
