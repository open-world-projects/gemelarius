import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageProjectionComponent } from './image-projection.component';
import { RouterModule } from '@angular/router';
import { ImageSceneModule } from '@components/image-scene/image-scene.module';
import { DetectorInputModule } from '@components/detector-input/detector-input.module';

@NgModule({
    declarations: [ImageProjectionComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([{ path: '', component: ImageProjectionComponent }]),
        ImageSceneModule,
        DetectorInputModule,
    ],
})
export class ImageProjectionModule {}
