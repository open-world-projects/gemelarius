import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnInit,
    ViewChild,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FigureAxisType, FigureType } from '@components/drag-figure/FigureType';
import { ProgressService } from '@services/progress.service';
import { Utils } from '@Utils';
import { DragWords, ListFigure, ListWord } from '@models/drag';

@Component({
    selector: 'app-drag',
    templateUrl: './drag.component.html',
    styleUrls: ['./drag.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DragComponent implements OnInit, AfterViewInit {
    public readonly FIND_WORDS: string;
    public readonly EXPECTED_WORD: string;

    public listFigure$: BehaviorSubject<ListFigure[]> = new BehaviorSubject<ListFigure[]>([]);
    public listWord$: BehaviorSubject<ListWord[]> = new BehaviorSubject<ListWord[]>([]);

    @ViewChild('field') field: ElementRef<HTMLElement> | undefined;

    constructor(private readonly _ref: ChangeDetectorRef, private readonly _progress: ProgressService) {
        this.FIND_WORDS = Utils.RandomKeyInEnum(DragWords);
        this.EXPECTED_WORD = this.FIND_WORDS.toLowerCase();
    }

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        const width = this.field?.nativeElement?.offsetWidth ?? 0;
        const height = this.field?.nativeElement?.offsetHeight ?? 0;

        // generate figure list
        const mas: ListFigure[] = [];
        for (let i = 0; i < width; i += 100) {
            for (let j = 0; j < height; j += 100) {
                mas.push({
                    axis: Utils.RandomValueFromObject(FigureAxisType),
                    type: FigureType.SQUARE,
                    x: i + 150 > width ? width - 150 : i,
                    y: j + 150 > height ? height - 150 : j,
                });
            }
        }

        // generate word list
        const masWord: ListWord[] = this.FIND_WORDS.split('').map((e) => ({
            word: e,
            x: Utils.RandomValueFromRange(0, width - 70),
            y: Utils.RandomValueFromRange(0, height - 70),
        }));

        this.listWord$.next(masWord);
        this.listFigure$.next(mas);

        // TODO: crutch
        this._ref.detectChanges();
    }

    success() {
        this._progress.success();
    }
}
