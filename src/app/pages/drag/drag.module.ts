import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragComponent } from './drag.component';
import { RouterModule } from '@angular/router';
import { DragFigureModule } from '@components/drag-figure/drag-figure.module';
import { DetectorInputModule } from '@components/detector-input/detector-input.module';

@NgModule({
    declarations: [DragComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([{ path: '', component: DragComponent }]),
        DragFigureModule,
        DetectorInputModule,
    ],
})
export class DragModule {}
