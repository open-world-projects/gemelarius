import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeDateComponent } from './change-date.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [ChangeDateComponent],
    imports: [CommonModule, RouterModule.forChild([{ path: '', component: ChangeDateComponent }])],
})
export class ChangeDateModule {}
