import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BehaviorSubject, filter, interval, takeUntil, timer } from 'rxjs';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-change-date',
    templateUrl: './change-date.component.html',
    styleUrls: ['./change-date.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
})
export class ChangeDateComponent implements OnInit {
    readonly SECOND_EXPECTED = 48;
    timer$: BehaviorSubject<string> = new BehaviorSubject<string>('');

    private _complete = false;

    constructor(private _destroy: DestroyService, private _progress: ProgressService) {}

    ngOnInit(): void {
        interval(1000)
            .pipe(takeUntil(this._destroy))
            .pipe(filter((e) => !this._complete))
            .subscribe(this.checkMoment.bind(this));
    }

    private checkMoment() {
        const second = new Date().getSeconds();
        if (second < this.SECOND_EXPECTED) {
            this.timer$.next(`${this.SECOND_EXPECTED - second} second`);
        } else if (second > this.SECOND_EXPECTED) {
            this.timer$.next(`${60 - second + this.SECOND_EXPECTED} second`);
        } else {
            this._complete = true;
            this.complete();
        }
    }

    private complete() {
        timer(10)
            .pipe(takeUntil(this._destroy))
            .subscribe(() => this.timer$.next('success'));
        timer(1000)
            .pipe(takeUntil(this._destroy))
            .subscribe(() => this._progress.success());
    }
}
