import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { GamepuzzleComponent } from './gamepuzzle.component';

const route: Route[] = [
  {
      path: '',
      component: GamepuzzleComponent,
  },
];

@NgModule({
  declarations: [GamepuzzleComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ]
})
export class GamepuzzleModule { }
