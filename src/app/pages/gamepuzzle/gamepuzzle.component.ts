import { Utils } from '@Utils';
import { Component, OnInit } from '@angular/core';
import { EmersionAnimation, PuzzleShift } from '@models/animation';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { takeUntil, timer } from 'rxjs';

interface Item {
    value: number;
    pos?: ItemPos;
}

interface ItemPos {
    i: number;
    j: number;
    str?: string;
}

const count = 4;

@Component({
    selector: 'app-gamepuzzle',
    templateUrl: './gamepuzzle.component.html',
    styleUrls: ['./gamepuzzle.component.scss'],
    providers: [DestroyService],
    animations: [EmersionAnimation, PuzzleShift(count)],
})
export class GamepuzzleComponent implements OnInit {
    game: boolean = true;
    size: number = count;

    moving = false;

    list: Item[] = [];

    private _finish: ItemPos[] = [];

    private _freePos!: ItemPos;

    constructor(
        private readonly destroy: DestroyService,
        private readonly _progress: ProgressService,
        private readonly _msg: NzMessageService
    ) {}

    ngOnInit(): void {
        for (let i = 1; i < this.size ** 2; i++) {
            this.list.push({ value: i });
        }

        Utils.Shuffle(this.list);

        for (let i = 1, countEl = 0; i <= this.size; i++) {
            for (let j = 1; j <= this.size && countEl < this.size ** 2 - 1; j++) {
                this.list[countEl++].pos = { i, j, str: `pos-${i}-${j}` };
                this._finish.push({ i, j });
            }
        }
        this._freePos = { i: this.size, j: this.size, str: `pos-${this.size}-${this.size}` };
    }

    startAnimation() {
        this.moving = true;
    }

    endAnimation() {
        this.check();
        this.moving = false;
    }

    move(item: Item) {
        if (this.moving) {
            return;
        }

        if (
            (item.pos?.i === this._freePos.i && Math.abs(item.pos?.j - this._freePos.j) === 1) ||
            (item.pos?.j === this._freePos.j && Math.abs(item.pos?.i - this._freePos.i) === 1)
        ) {
            [item.pos, this._freePos] = [{ ...this._freePos }, { ...item.pos }];
        } else {
            this._msg.warning('This bad try!');
        }
    }

    private check() {
        const temp = [...this.list];
        temp.sort((a, b) => a.value - b.value);
        const finish = temp.every((e, i) => e.pos?.i === this._finish[i].i && e.pos.j === this._finish[i].j);
        finish &&
            ((this.game = false),
            timer(1200)
                .pipe(takeUntil(this.destroy))
                .subscribe((e) => this._progress.success()));
    }
}
