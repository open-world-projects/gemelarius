import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { takeUntil, timer } from 'rxjs';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';
import { Utils } from '@Utils';

interface Pare {
    icon: string;
    show?: boolean;
}

@Component({
    selector: 'app-findpare',
    templateUrl: './findpare.component.html',
    styleUrls: ['./findpare.component.scss'],
    providers: [DestroyService],
})
export class FindpareComponent implements OnInit {
    mas: Pare[] = [];
    game: boolean = true;
    choosen: Pare | null = null;
    count: number = 0;

    constructor(
        private readonly _progress: ProgressService,
        private readonly _destroy: DestroyService,
        private readonly _ref: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.mas = ['trophy', 'smile', 'trademark-circle', 'meh', 'dashboard', 'bug', 'car', 'camera', 'bank', 'crown']
            .map((icon) => [{ icon }, { icon }])
            .flat();

        Utils.Shuffle<Pare>(this.mas);
    }

    showimg(e: Pare) {
        if (e.show) {
            return;
        }
        e.show = true;
        if (!this.choosen) {
            this.choosen = e;
        } else {
            const prev = this.choosen;
            this.choosen = null;
            if (prev.icon !== e.icon) {
                timer(400)
                    .pipe(takeUntil(this._destroy))
                    .subscribe(() => {
                        [e.show, prev.show] = [false, false];
                        this._ref.detectChanges();
                    });
            }
        }
        this.check();
    }

    check() {
        if (this.mas.every((current) => current.show)) {
            this.game = false;
            timer(1000)
                .pipe(takeUntil(this._destroy))
                .subscribe(() => this._progress.success());
        }
    }
}
