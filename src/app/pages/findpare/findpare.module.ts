import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FindpareComponent } from './findpare.component';
import { RouterModule } from '@angular/router';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
    declarations: [FindpareComponent],
    imports: [CommonModule, NzIconModule, RouterModule.forChild([{ path: '', component: FindpareComponent }])],
})
export class FindpareModule {}
