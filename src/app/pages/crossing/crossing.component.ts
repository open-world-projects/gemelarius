import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-crossing',
    templateUrl: './crossing.component.html',
    styleUrls: ['./crossing.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CrossingComponent implements OnInit {
    constructor(private _progress: ProgressService) {}

    ngOnInit(): void {}

    complete() {
        this._progress.success();
    }
}
