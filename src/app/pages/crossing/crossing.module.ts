import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrossingComponent } from './crossing.component';
import { RouterModule } from '@angular/router';
import { CrossingBaseModule } from '@components/crossing/crossing-base/crossing-base.module';

@NgModule({
    declarations: [CrossingComponent],
    imports: [CommonModule, RouterModule.forChild([{ path: '', component: CrossingComponent }]), CrossingBaseModule],
})
export class CrossingModule {}
