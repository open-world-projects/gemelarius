import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CheckProgressGuard } from '@guards/check-progress.guard';
import { RouteApp } from '@models/common/RouteApp';
import { MainComponent } from './main.component';

const routes: Route[] = [
    {
        path: '',
        component: MainComponent,
        canActivateChild: [CheckProgressGuard],
        children: [
            {
                path: RouteApp.app?.drag?.short,
                loadChildren: () => import('../../pages/drag/drag.module').then((e) => e.DragModule),
                data: { name: RouteApp.app?.drag?.name, animationState: RouteApp.app?.drag?.short },
            },
            {
                path: RouteApp.app?.bubbleNumber?.short,
                loadChildren: () =>
                    import('../../pages/bubble-number/bubble-number.module').then((e) => e.BubbleNumberModule),
                data: { name: RouteApp.app?.bubbleNumber?.name, animationState: RouteApp.app?.bubbleNumber?.short },
            },
            {
                path: RouteApp.app?.changeDate?.short,
                loadChildren: () =>
                    import('../../pages/change-date/change-date.module').then((e) => e.ChangeDateModule),
                data: { name: RouteApp.app?.changeDate?.name, animationState: RouteApp.app?.changeDate?.short },
            },
            {
                path: RouteApp.app?.imageProjection?.short,
                loadChildren: () =>
                    import('../../pages/image-projection/image-projection.module').then((e) => e.ImageProjectionModule),
                data: {
                    name: RouteApp.app?.imageProjection?.name,
                    animationState: RouteApp.app?.imageProjection?.short,
                },
            },
            {
                path: RouteApp.app?.mobileSize?.short,
                loadChildren: () =>
                    import('../../pages/mobile-size/mobile-size.module').then((e) => e.MobileSizeModule),
                data: { name: RouteApp.app?.mobileSize?.name, animationState: RouteApp.app?.mobileSize?.short },
            },
            {
                path: RouteApp.app?.numberTree?.short,
                loadChildren: () =>
                    import('../../pages/number-tree/number-tree.module').then((e) => e.NumberTreeModule),
                data: { name: RouteApp.app?.numberTree?.name, animationState: RouteApp.app?.numberTree?.short },
            },
            {
                path: RouteApp.app?.trifoliateCubes?.short,
                loadChildren: () =>
                    import('../../pages/trifoliate-cubes/trifoliate-cubes.module').then((e) => e.TrifoliateCubesModule),
                data: {
                    name: RouteApp.app?.trifoliateCubes?.name,
                    animationState: RouteApp.app?.trifoliateCubes?.short,
                },
            },
            {
                path: RouteApp.app?.crossing?.short,
                loadChildren: () => import('../../pages/crossing/crossing.module').then((e) => e.CrossingModule),
                data: { name: RouteApp.app?.crossing?.name, animationState: RouteApp.app?.crossing?.short },
            },
            {
                path: RouteApp.app?.hanoi?.short,
                loadChildren: () =>
                    import('../../pages/towers-of-hanoi/towers-of-hanoi.module').then((e) => e.TowersOfHanoiModule),
                data: { name: RouteApp.app?.hanoi?.name, animationState: RouteApp.app?.hanoi?.short },
            },
            {
                path: RouteApp.app?.findPare?.short,
                loadChildren: () => import('../../pages/findpare/findpare.module').then((e) => e.FindpareModule),
                data: {
                    name: RouteApp.app?.findPare?.name,
                    animationState: RouteApp.app?.findPare?.short,
                },
            },
            {
                path: RouteApp.app?.gamePuzzle?.short,
                loadChildren: () => import('../../pages/gamepuzzle/gamepuzzle.module').then((e) => e.GamepuzzleModule),
                data: {
                    name: RouteApp.app?.gamePuzzle?.name,
                    animationState: RouteApp.app?.gamePuzzle?.short,
                },
            },
            {
                path: RouteApp.app?.cycleNumber?.short,
                loadChildren: () =>
                    import('../../pages/cycle-number/cycle-number.module').then((e) => e.CycleNumberModule),
                data: {
                    name: RouteApp.app?.cycleNumber?.name,
                    animationState: RouteApp.app?.cycleNumber?.short,
                },
            },
            {
                path: RouteApp.app?.sudoku?.short,
                loadChildren: () => import('../../pages/sudoku/sudoku.module').then((e) => e.SudokuModule),
                data: {
                    name: RouteApp.app?.sudoku?.name,
                    animationState: RouteApp.app?.sudoku?.short,
                },
            },
            {
                path: RouteApp.app?.resort?.short,
                loadComponent: () => import('../../pages/resort/resort.component').then((c) => c.ResortComponent),
                data: {
                    name: RouteApp.app?.resort?.name,
                    animationState: RouteApp.app?.resort?.short,
                },
            },
            {
                path: RouteApp.app?.points?.short,
                loadComponent: () => import('../../pages/points/points.component').then((c) => c.PointsComponent),
                data: {
                    name: RouteApp.app?.points?.name,
                    animationState: RouteApp.app?.points?.short,
                },
            },
            {
                path: RouteApp.app?.toBeContinue?.short,
                loadChildren: () =>
                    import('../../pages/to-be-continue/to-be-continue.module').then((e) => e.ToBeContinueModule),
                data: { name: RouteApp.app?.toBeContinue?.name, animationState: RouteApp.app?.toBeContinue?.short },
            },
            {
                path: '**',
                redirectTo: RouteApp.app?.drag?.short,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MainRoutingModule {}
