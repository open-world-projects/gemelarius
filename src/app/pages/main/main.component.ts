import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, RouterOutlet } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BehaviorSubject, filter, takeUntil } from 'rxjs';
import { routeTransitionAnimations } from '@models/animation';
import { RouteApp, RouteType } from '@models/common/RouteApp';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';
import { StateSaverService } from '@services/state-saver.service';
import { RouteTypeList } from '@models/RouteTypeList';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
    animations: [routeTransitionAnimations],
})
export class MainComponent implements OnInit {
    readonly TITLE_APP = 'Gemelarius';

    listPages$: BehaviorSubject<RouteTypeList[]> = new BehaviorSubject<RouteTypeList[]>([]);

    constructor(
        private readonly _activatedRoute: ActivatedRoute,
        private readonly _destroy: DestroyService,
        private readonly _titleService: Title,
        private readonly _router: Router,
        private readonly _progress: ProgressService,
        private readonly _state: StateSaverService
    ) {}

    ngOnInit(): void {
        this.setTitlePage();
        this.setListPage();
        this._router.events
            .pipe(takeUntil(this._destroy))
            .pipe(filter((event) => event instanceof NavigationEnd))
            .subscribe(() => (this.setTitlePage(), this.setListPage()));

        this._progress.successStep
            .pipe(takeUntil(this._destroy))
            .subscribe(() => (this.successStep(), this.setListPage()));
    }

    selectPage(page: RouteTypeList): void {
        this._router.navigate([page.path]);
    }

    successStep() {
        const path: string = this._router.url.split('/').pop() ?? '';
        this._state.saveProgress(path);

        const paths = Object.values(RouteApp.app ?? {}).filter((e) => e?.path);
        const indexRoute = paths.findIndex((e) => e.short === path);
        ~indexRoute && this._router.navigate([paths[indexRoute + 1].path]);
    }

    prepareRoute(outlet: RouterOutlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animationState'];
    }

    private setTitlePage() {
        const data = this._activatedRoute.snapshot.firstChild?.data;
        const name = data?.['name'];
        name && this._titleService.setTitle(name);
    }

    private setListPage() {
        const data = this._activatedRoute.snapshot.firstChild?.data;
        const currentPage = data?.['name'];
        const state = this._state.loadProgress();
        const pages: RouteTypeList[] = Object.values(RouteApp.app ?? {})
            .filter((e) => e?.path)
            .map((e: RouteType, i: number) => ({
                ...e,
                disabled: e.name === currentPage || (state?.page < i && environment.production),
                selected: e.name === currentPage,
            }));
        this.listPages$.next(pages);
    }
}
