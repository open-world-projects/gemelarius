import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { SelectPageModule } from '@components/select-page/select-page.module';

@NgModule({
    declarations: [MainComponent],
    imports: [CommonModule, MainRoutingModule, NzButtonModule, NzIconModule, SelectPageModule],
})
export class MainModule {}
