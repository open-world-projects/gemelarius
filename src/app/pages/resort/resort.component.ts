import { CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, DestroyRef, inject, signal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { UpperCasePipe } from '@angular/common';
import { EmersionAnimation } from '@models/animation';
import { ProgressService } from '@services/progress.service';
import { Utils } from '@Utils';
import { timer } from 'rxjs';

const listResortWords = [
    'picture',
    'happiness',
    'butterfly',
    'adventure',
    'education',
    'emergency',
    'excellent',
    'knowledge',
    'marketing',
];

@Component({
    selector: 'app-resort',
    standalone: true,
    imports: [CdkDropList, CdkDrag, UpperCasePipe],
    templateUrl: './resort.component.html',
    styleUrl: './resort.component.scss',
    animations: [EmersionAnimation],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResortComponent {
    private readonly progress = inject(ProgressService);
    private readonly destroyRef = inject(DestroyRef);
    readonly word: string;
    readonly shuffleWord: string[];
    end = false;
    done = signal(false);
    constructor() {
        Utils.Shuffle(listResortWords);
        this.word = listResortWords[0];
        const letters = this.word.split('');
        Utils.Shuffle(letters);
        this.shuffleWord = letters;
    }

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.shuffleWord, event.previousIndex, event.currentIndex);

        if (this.shuffleWord.join('') === this.word) {
            this.end = true;
            timer(1500)
                .pipe(takeUntilDestroyed(this.destroyRef))
                .subscribe(() => this.progress.success());
        }
    }
}
