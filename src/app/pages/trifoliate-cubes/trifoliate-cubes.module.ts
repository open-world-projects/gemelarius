import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TrifoliateCubesComponent } from './trifoliate-cubes.component';
import { TrifoliateCubesBaseModule } from '@components/trifoliate-cubes/trifoliate-cubes-base/trifoliate-cubes-base.module';

@NgModule({
    declarations: [TrifoliateCubesComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([{ path: '', component: TrifoliateCubesComponent }]),
        TrifoliateCubesBaseModule,
    ],
})
export class TrifoliateCubesModule {}
