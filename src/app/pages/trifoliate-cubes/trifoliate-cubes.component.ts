import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { takeUntil, timer } from 'rxjs';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-trifoliate-cubes',
    templateUrl: './trifoliate-cubes.component.html',
    styleUrls: ['./trifoliate-cubes.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
})
export class TrifoliateCubesComponent implements OnInit {
    constructor(private _destroy: DestroyService, private _progress: ProgressService) {}

    ngOnInit(): void {}

    complete() {
        timer(1000).pipe(takeUntil(this._destroy)).subscribe(this._progress.success.bind(this._progress));
    }
}
