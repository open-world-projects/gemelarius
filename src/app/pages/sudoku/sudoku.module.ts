import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SudokuComponent } from './sudoku.component';
import { RouterModule } from '@angular/router';
import { SudokuGridComponent } from './grid/sudoku-grid.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
    declarations: [SudokuComponent, SudokuGridComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([{ path: '', component: SudokuComponent }]),
        NzSpinModule,
        NzIconModule
    ],
})
export class SudokuModule {}
