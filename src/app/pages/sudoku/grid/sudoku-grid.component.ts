import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Sudoku, SudokuCoordinates, SudokuField } from '../common/sudoku';

@Component({
    selector: 'sudoku-grid',
    templateUrl: './sudoku-grid.component.html',
    styleUrls: ['./sudoku-grid.component.scss'],
})
export class SudokuGridComponent {
    @Input() sudoku?: Sudoku;
    @Input() activeField?: SudokuField;
    @Input() currentCoordinates: SudokuCoordinates = new SudokuCoordinates();

    @Output() activeFieldChange = new EventEmitter<SudokuField>();

    possibleAnswers: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    onFieldClick(field: SudokuField) {
        this.activeField = this.activeField === field ? undefined : field;

        // @ts-ignore
        this.currentCoordinates.row = this.sudoku?.findIndex(row => row.indexOf(this.activeField) !== -1);
        // @ts-ignore
        this.currentCoordinates.col = this.sudoku[Math.max(this.currentCoordinates.row, 0)].indexOf(this.activeField);

        this.activeFieldChange.emit(this.activeField);
    }
}
