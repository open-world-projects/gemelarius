import { SudokuGenerator } from './common/sudokuGenerator';

addEventListener('message', ({ data }) => {
    let sudoku = SudokuGenerator.generateSudoku(data.difficult);
    postMessage({ sudoku });
});
