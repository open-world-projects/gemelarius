import { ChangeDetectionStrategy, Component, HostListener } from '@angular/core';
import {
    Sudoku,
    SudokuCoordinates,
    SudokuDifficult,
    SudokuField,
    SudokuMove,
    SudokuFreeNumber,
    LiveInSudoku,
} from './common/sudoku';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BehaviorSubject, takeUntil, timer } from 'rxjs';
import { SudokuGenerator } from './common/sudokuGenerator';
import { Utils } from '@Utils';

@Component({
    selector: 'app-sudoku',
    templateUrl: './sudoku.component.html',
    styleUrls: ['./sudoku.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SudokuComponent {

    sudoku?: Sudoku;
    activeField?: SudokuField;
    currentCoordinates: SudokuCoordinates;
    noteMode: boolean = false;
    sudokuGeneration: boolean = false;
    selectedValue?: number;
    leftFreeNumbers: SudokuFreeNumber[] = [];

    isComplete$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    isLose$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    readonly lives: LiveInSudoku[] = [new LiveInSudoku(), new LiveInSudoku(), new LiveInSudoku()];
    private countOfLives: number = this.lives.length;

    readonly EASY_DIFFICULT: SudokuDifficult = SudokuDifficult.EASY;
    readonly MEDIUM_DIFFICULT: SudokuDifficult = SudokuDifficult.MEDIUM;
    readonly HARD_DIFFICULT: SudokuDifficult = SudokuDifficult.HARD;
    readonly EXPERT_DIFFICULT: SudokuDifficult = SudokuDifficult.EXPERT;

    private readonly historyOfMoves: SudokuMove[] = [];

    constructor(private readonly _destroy: DestroyService,
                private readonly _progress: ProgressService,
                private readonly _msg: NzMessageService) {
        this.currentCoordinates = new SudokuCoordinates();
        for (let i = 1; i <= 9; i++) {
            this.leftFreeNumbers.push({
                number: i,
                leftFreeCount: 9,
            });
        }
    }

    public generateSudoku(difficult: SudokuDifficult): void {
        this.sudokuGeneration = true;
        this.isComplete$.next(false);

        if (typeof Worker !== 'undefined') {
            let worker = new Worker(new URL('./sudoku.worker', import.meta.url));
            worker.onmessage = ({ data }) => {
                this.sudoku = data.sudoku;
                this.checkNumbers();
                this.sudokuGeneration = false;
                this.isComplete$.next(false);
            };
            worker.postMessage({ difficult });
        } else {
            setTimeout(() => {
                this.sudoku = SudokuGenerator.generateSudoku(difficult);
                this.checkNumbers();
                this.sudokuGeneration = false;
                this.isComplete$.next(false);
            }, 100);
        }
    }

    selectValue(number: number): void {
        this.selectedValue = this.selectedValue === number ? undefined : number;
        if (this.selectedValue) {
            this.activeField = new SudokuField();
            this.activeField.value = number;
            this.currentCoordinates = { row: -1, col: -1 };
        } else {
            this.activeField = undefined;
        }
    }

    @HostListener('window:keydown.space')
    private onSpace() {
        this.noteMode = !this.noteMode;
    }

    @HostListener('window:keydown.escape')
    private onEscape() {
        this.currentCoordinates.row = -1;
        this.currentCoordinates.col = -1;
        this.activeField = undefined;
    }

    @HostListener('window:keydown.arrowUp')
    private onArrowUp(): void {
        this.moveFocus(-1, 0);
    }

    @HostListener('window:keydown.arrowDown')
    private onArrowDown(): void {
        this.moveFocus(1, 0);
    }

    @HostListener('window:keydown.arrowLeft')
    private onArrowLeft(): void {
        this.moveFocus(0, -1);
    }

    @HostListener('window:keydown.arrowRight')
    private onArrowRight(): void {
        this.moveFocus(0, 1);
    }

    @HostListener('window:keydown.backspace')
    private onBackspace(): void {
        this.erase();
    }

    @HostListener('window:keydown', ['$event'])
    private onKeyDown(event: KeyboardEvent): void {
        if (event.code == 'KeyZ' && (event.ctrlKey || event.metaKey)) {
            this.undoMove();
            return;
        }

        const number = parseInt(event.key, 10);

        if (!this.activeField || isNaN(number) || number < 1 || number > 9) {
            return;
        }

        this.insertNumber(number);
    }

    private moveFocus(relativeRow: number, relativeCol: number): void {
        if (this.sudoku) {
            this.currentCoordinates.row = Math.min(Math.max(this.currentCoordinates.row + relativeRow, 0), 8);
            this.currentCoordinates.col = Math.min(Math.max(this.currentCoordinates.col + relativeCol, 0), 8);
            this.activeField = this.sudoku[this.currentCoordinates.row][this.currentCoordinates.col];
        }
    }

    private erase(): void {
        if (!this.activeField || this.activeField.readonly || this.isLose$.value) {
            return;
        }

        this.activeField.isMistake = false;
        this.activeField.value = undefined;
        this.checkNumbers();
    }

    private insertNumber(val: number): void {
        if (!this.activeField || this.activeField.readonly || this.isLose$.value) {
            return;
        }
        this.addMove();

        if (this.noteMode) {
            let valIndexInNotes = this.activeField.notes.indexOf(val);
            if (valIndexInNotes === -1) {
                this.activeField.notes.push(val);
            } else {
                this.activeField.notes.splice(valIndexInNotes, 1);
            }
        } else if (val === this.activeField.value) {
            this.erase();
        } else {
            this.activeField.value = val;
            this.activeField.isMistake = val !== this.activeField.answer;
            if (this.activeField.isMistake) {
                this.countOfLives--;
                if (this.countOfLives >= 0) {
                    this.lives[this.countOfLives].isAlive = false;
                }
            }
            this.activeField.notes = [];
            this.cleanNotes();
            this.checkNumbers();
            this.checkFinished();
            this.checkLose();
        }
    }

    private cleanNotes() {
        if (this.sudoku && this.activeField && this.activeField.value) {
            let newValue = this.activeField.value;
            this.sudoku[this.currentCoordinates.row]
                .flatMap(row => row)
                .filter(field => !field.readonly && field.notes.includes(newValue))
                .forEach(field => field.notes.splice(field.notes.indexOf(newValue), 1));
            this.sudoku
                .flatMap(row => row[this.currentCoordinates.col])
                .filter(field => !field.readonly && field.notes.includes(newValue))
                .forEach(field => field.notes.splice(field.notes.indexOf(newValue), 1));

            let sectionXIndex = Math.floor(this.currentCoordinates.row / 3);
            let sectionYIndex = Math.floor(this.currentCoordinates.col / 3);
            this.sudoku
                .filter((row, index) => index >= sectionXIndex * 3 && index < (sectionXIndex + 1) * 3)
                .flatMap(row =>
                    row.filter((value, index) => index >= sectionYIndex * 3 && index < (sectionYIndex + 1) * 3))
                .forEach(field => field.notes.splice(field.notes.indexOf(newValue), 1));
        }
    }

    private checkNumbers() {
        this.leftFreeNumbers.forEach(v => v.leftFreeCount = 9);
        if (this.sudoku) {
            this.sudoku
                .flatMap(row => row)
                .filter(field => field.value)
                .forEach(v => {
                    // @ts-ignore
                    this.leftFreeNumbers[v.value - 1].leftFreeCount--;
                });
        }
    }

    private checkFinished(): void {
        if (this.finished) {
            this.isComplete$.next(true);
            timer(1000).pipe(takeUntil(this._destroy)).subscribe(this._progress.success.bind(this._progress));
        }
    }

    private checkLose() {
        if (this.countOfLives < 0) {
            this.isLose$.next(true);
        }
    }

    private get finished(): boolean {
        return this.sudoku !== undefined && this.sudoku.every(row => row.every(field => field.value === field.answer));
    }

    private addMove() {
        this.historyOfMoves.push({
            sudoku: Utils.copyObject(this.sudoku),
            currentCoordinates: Utils.copyObject(this.currentCoordinates),
        });
    }

    private undoMove() {
        if (this.sudoku && !this.finished && !this.isLose$.value) {
            let sudokuMove = this.historyOfMoves.pop();
            if (sudokuMove !== undefined) {
                this.sudoku = sudokuMove.sudoku;
                // @ts-ignore
                this.currentCoordinates = sudokuMove.currentCoordinates;
                this.activeField = (this.sudoku ?? [])[this.currentCoordinates.row][this.currentCoordinates.col];
            }
        }
    }
}
