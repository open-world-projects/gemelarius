export type Sudoku = SudokuField[][];

export class SudokuField {
    value?: number;
    notes: number[];
    answer?: number;
    readonly?: boolean;
    isMistake: boolean = false;

    constructor() {
        this.notes = [];
    }
}

export class SudokuCoordinates {
    row: number = -1;
    col: number = -1;
}

export class LiveInSudoku {
    isAlive: boolean = true;
}

export interface SudokuMove {
    sudoku?: Sudoku;
    currentCoordinates?: SudokuCoordinates;
}

export interface SudokuFreeNumber {
    number: number;
    leftFreeCount: number;
}

export enum SudokuDifficult {
    EASY = 35,
    MEDIUM = 45,
    HARD = 53,
    EXPERT = 60
}
