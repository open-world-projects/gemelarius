import { SudokuCoordinates, SudokuField } from './sudoku';
import { Utils } from '@Utils';

export class SudokuSolver {

    public static possibleValues: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    public static solve(sudoku: SudokuField[][], field: string, direction?: number): number {
        let countOfSudokuSolution = 0;
        for (let steps = 0; this.updateNotesAndSolveSingle(sudoku, field) && steps < 81; steps++) {}
        if (this.isFilled(sudoku, field)) {
            countOfSudokuSolution = 1;
        }
        if (!this.isFilled(sudoku, field) && !this.isFailed(sudoku, field)) {
            let firstSudokuCopy;
            let secondSudokuCopy;
            if (!direction || direction > 0) {
                firstSudokuCopy = Utils.copyObject(sudoku);
                this.backtracking(firstSudokuCopy, field, 1);
            }
            if (!direction || direction < 0) {
                secondSudokuCopy = Utils.copyObject(sudoku);
                this.backtracking(secondSudokuCopy, field, -1);
            }
            if (firstSudokuCopy || secondSudokuCopy) {
                countOfSudokuSolution = 1;
                if (firstSudokuCopy && secondSudokuCopy) {
                    // @ts-ignore
                    let firstSudokuResult = JSON.stringify(firstSudokuCopy.map(v => v.map(v => v[field])));
                    // @ts-ignore
                    let secondSudokuResult = JSON.stringify(secondSudokuCopy.map(v => v.map(v => v[field])));
                    countOfSudokuSolution = firstSudokuResult === secondSudokuResult ? 1 : 2;
                }
                let sudokuResult = firstSudokuCopy ?? secondSudokuCopy;
                for (let row = 0; row < sudoku.length; row++) {
                    // @ts-ignore
                    sudoku[row] = sudokuResult[row];
                }
            }
        }
        return countOfSudokuSolution;
    }

    public static findNotes(sudoku: SudokuField[][], field: string, row: number, col: number): number[] {
        // @ts-ignore
        let rowUsedValues = sudoku[row].flatMap(sudokuField => sudokuField[field]);
        let freeValuesOnRow = this.possibleValues.filter(value => !rowUsedValues.includes(value));

        // @ts-ignore
        let columnUsedValues = sudoku.flatMap(sudokuRow => sudokuRow[col][field]);
        let freeValuesOnColumn = this.possibleValues.filter(value => !columnUsedValues.includes(value));

        let sectionXIndex = Math.floor(row / 3);
        let sectionYIndex = Math.floor(col / 3);
        let sectionUsedValues = sudoku
            .filter((row, index) => index >= sectionXIndex * 3 && index < (sectionXIndex + 1) * 3)
            .flatMap(
                row =>
                    row
                        .filter((value, index) => index >= sectionYIndex * 3 && index < (sectionYIndex + 1) * 3)
                        // @ts-ignore
                        .flatMap(value => value[field]),
            );
        let freeValuesOnSection = this.possibleValues.filter(value => !sectionUsedValues.includes(value));

        return freeValuesOnRow
            .filter(value => freeValuesOnColumn.includes(value) && freeValuesOnSection.includes(value));
    }

    public static updateNotesAndSolveSingle(sudoku: SudokuField[][], field: string): number {
        let changed = 0;
        for (let i = 0, row = 0, col = 0; i < 81; i++, row = Math.floor(i / 9), col = i % 9) {
            if (sudoku[row][col].readonly === undefined) {
                let solveSingleChanged = this.solveSingle(sudoku, field, row, col);
                changed += solveSingleChanged;
                if (solveSingleChanged === 0) {
                    changed += this.solveHiddenSingle(sudoku, field, row, col);
                }
            }
        }
        return changed;
    }

    private static backtracking(sudoku: SudokuField[][], field: string, direction: number): void {
        let minNotesCoordinates = new SudokuCoordinates();
        for (let i = 0, row = 0, col = 0, minNotesCount = 10; i < 81; i++, row = Math.floor(i / 9), col = i % 9) {
            // @ts-ignore
            if (sudoku[row][col][field] === undefined && sudoku[row][col].notes.length < minNotesCount) {
                minNotesCoordinates.row = row;
                minNotesCoordinates.col = col;
                minNotesCount = sudoku[row][col].notes.length;
            }
        }

        let sudokuField = sudoku[minNotesCoordinates.row][minNotesCoordinates.col];
        let element = (sudokuField.notes.length + direction) % (sudokuField.notes.length + 1);
        for (let i = 0; i < sudokuField.notes.length; i++, element += direction) {
            // @ts-ignore
            sudokuField[field] = sudokuField.notes[element];
            let copyOfSudoku = Utils.copyObject(sudoku);
            this.solve(copyOfSudoku, field, direction);
            if (this.isFilled(copyOfSudoku, field)) {
                for (let j = 0, row = 0, col = 0; j < 81; j++, row = Math.floor(j / 9), col = j % 9) {
                    // @ts-ignore
                    this.markSolvedField(sudoku, field, row, col, copyOfSudoku[row][col][field]);
                }
                return;
            }
        }
    }

    private static solveSingle(sudoku: SudokuField[][], field: string, row: number, col: number): number {
        let sudokuField = sudoku[row][col];
        sudokuField.notes = this.findNotes(sudoku, field, row, col);
        if (sudokuField.notes.length === 1) {
            this.markSolvedField(sudoku, field, row, col, sudokuField.notes[0]);
            return 1;
        }
        return 0;
    }

    private static solveHiddenSingle(sudoku: SudokuField[][], field: string, row: number, col: number): number {
        let sudokuField = sudoku[row][col];
        this.reduceRowNotes(sudoku, row, col);
        if (sudokuField.notes.length === 1) {
            this.markSolvedField(sudoku, field, row, col, sudokuField.notes[0]);
            return 1;
        }
        this.reduceColumnNotes(sudoku, row, col);
        if (sudokuField.notes.length === 1) {
            this.markSolvedField(sudoku, field, row, col, sudokuField.notes[0]);
            return 1;
        }
        this.reduceSectionNotes(sudoku, row, col);
        if (sudokuField.notes.length === 1) {
            this.markSolvedField(sudoku, field, row, col, sudokuField.notes[0]);
            return 1;
        }
        return 0;
    }

    private static reduceRowNotes(sudoku: SudokuField[][], row: number, col: number): void {
        let sudokuField = sudoku[row][col];
        let sudokuFieldNotes = Utils.copyObject(sudokuField.notes);
        for (let i = 0; i < 9; i++) {
            if (i !== col || sudoku[row][i].readonly === undefined) {
                sudokuFieldNotes = sudokuFieldNotes.filter(v => !sudoku[row][i].notes.includes(v));
            }
        }
        if (sudokuFieldNotes.length === 1) {
            sudokuField.notes = sudokuFieldNotes;
        }
    }

    private static reduceColumnNotes(sudoku: SudokuField[][], row: number, col: number): void {
        let sudokuField = sudoku[row][col];
        let sudokuFieldNotes = Utils.copyObject(sudokuField.notes);
        for (let i = 0; i < 9; i++) {
            if (i !== row || sudoku[i][col].readonly === undefined) {
                sudokuFieldNotes = sudokuFieldNotes.filter(v => !sudoku[i][col].notes.includes(v));
            }
        }
        if (sudokuFieldNotes.length === 1) {
            sudokuField.notes = sudokuFieldNotes;
        }
    }

    private static reduceSectionNotes(sudoku: SudokuField[][], row: number, col: number): void {
        let sudokuField = sudoku[row][col];
        let sudokuFieldNotes = Utils.copyObject(sudokuField.notes);

        let sectionXIndex = Math.floor(row / 3);
        let sectionYIndex = Math.floor(col / 3);
        let sectionFields = sudoku
            .filter((row, index) => index >= sectionXIndex * 3 && index < (sectionXIndex + 1) * 3)
            .flatMap(row =>
                row
                    .filter((value, index) => index >= sectionYIndex * 3 && index < (sectionYIndex + 1) * 3)
                    .flatMap(val => val),
            );

        for (let i = 0; i < sectionFields.length; i++) {
            if (sectionFields[i] !== sudokuField || sectionFields[i].readonly === undefined) {
                sudokuFieldNotes = sudokuFieldNotes.filter(v => !sudoku[row][i].notes.includes(v));
            }
        }
        if (sudokuFieldNotes.length === 1) {
            sudokuField.notes = sudokuFieldNotes;
        }
    }

    private static markSolvedField(sudoku: SudokuField[][],
                                   field: string,
                                   row: number,
                                   col: number,
                                   val: number): void {
        let sudokuField = sudoku[row][col];
        sudokuField.notes = [];
        sudokuField.readonly = true;
        // @ts-ignore
        sudokuField[field] = val;
    }

    private static isFilled(sudoku: SudokuField[][], field: string): boolean {
        let sudokuFields = sudoku.flatMap(row => row);
        for (let i = 0; i < sudokuFields.length; i++) {
            // @ts-ignore
            if (sudokuFields[i][field] === undefined) {
                return false;
            }
        }
        return true;
    }

    private static isFailed(sudoku: SudokuField[][], field: string): boolean {
        let sudokuFields = sudoku.flatMap(row => row);
        for (let i = 0; i < sudokuFields.length; i++) {
            // @ts-ignore
            if (sudokuFields[i][field] === undefined && sudokuFields[i].notes.length === 0) {
                return true;
            }
        }
        return false;
    }
}
