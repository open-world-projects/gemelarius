import { SudokuDifficult, SudokuField } from './sudoku';
import { Utils } from '@Utils';
import { SudokuSolver } from './sudokuSolver';

export class SudokuGenerator {

    public static values: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    static generateSudoku(difficult: SudokuDifficult): SudokuField[][] {
        const sudoku = new Array(9)
            .fill(undefined)
            .map(() => new Array(9).fill(undefined).map(() => new SudokuField()));
        this.fillMainDiagonal(sudoku);
        this.fillNonMainDiagonal(sudoku);
        this.clearFields(sudoku, difficult);
        return sudoku;
    }

    private static fillMainDiagonal(sudoku: SudokuField[][]): void {
        for (let i = 0; i < 3; i++) {
            this.fillSquare(sudoku, i, i);
        }
    }

    private static fillNonMainDiagonal(sudoku: SudokuField[][]): void {
        for (let row = 0; row < 9; row++) {
            for (let column = 0, route = 1; column < 9; column += route) {
                if (column < 0) {
                    row--;
                    column = 8;
                }
                if (Math.floor(row / 3) === Math.floor(column / 3)) {
                    continue;
                }
                try {
                    SudokuGenerator.generateFieldAnswer(sudoku, row, column);
                    route = 1;
                } catch (ex) {
                    route = -1;
                }
            }
        }
    }

    private static fillSquare(sudoku: SudokuField[][], indexXSquare: number, indexYSquare: number): void {
        for (let row = indexYSquare * 3; row < indexYSquare * 3 + 3; row++) {
            for (let column = indexXSquare * 3; column < indexXSquare * 3 + 3; column++) {
                try {
                    SudokuGenerator.generateFieldAnswer(sudoku, row, column);
                } catch (ex) {
                    column -= 2;
                }
            }
        }
    }

    private static generateFieldAnswer(sudoku: SudokuField[][], row: number, column: number): void {
        if (sudoku[row][column].notes.length === 0) {
            sudoku[row][column].notes = SudokuSolver.findNotes(sudoku, 'answer', row, column);
        } else {
            // @ts-ignore
            sudoku[row][column].notes.splice(sudoku[row][column].notes.indexOf(sudoku[row][column].answer), 1);
        }

        let freeValues = sudoku[row][column].notes ?? [];
        if (freeValues.length === 0) {
            sudoku[row][column].notes = [];
            sudoku[row][column].answer = undefined;
            throw `Error on generate sudoku field answer to row:column (${row}:${column})`;
        }

        sudoku[row][column].answer = freeValues[Utils.RandomValueFromRange(0, freeValues.length)];
        sudoku[row][column].readonly = false;
        sudoku[row][column].value = undefined;
    }

    private static clearFields(sudoku: SudokuField[][], difficult: SudokuDifficult): void {
        sudoku.forEach(row => row.forEach(field => {
            field.value = field.answer;
            field.notes = [];
        }));
        let notCheckedFields = sudoku.flatMap(v => v);
        for (let i = 0; i < difficult; i++) {
            let randomField = notCheckedFields.splice(Utils.RandomValueFromRange(0, notCheckedFields.length), 1)[0];
            if (randomField === undefined) {
                break;
            }
            if (randomField.value && !randomField.readonly) {
                randomField.value = undefined;
                randomField.readonly = false;
                if (this.hasMultipleSolution(sudoku)) {
                    randomField.value = randomField.answer;
                    randomField.readonly = true;
                    i--;
                    if (i < 0) {
                        i = difficult;
                    }
                }
            } else {
                i--;
            }
        }
        sudoku.forEach(row => row.forEach(field => {
            field.readonly = field.value !== undefined;
        }));
    }

    private static hasMultipleSolution(sudoku: SudokuField[][]): boolean {
        let sudokuCopy = Utils.copyObject(sudoku);
        sudokuCopy.forEach(v => v.forEach(v => v.readonly = v.value ? true : undefined));
        return SudokuSolver.solve(sudokuCopy, 'value') > 1;
    }
}
