import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TowersOfHanoiComponent } from './towers-of-hanoi.component';
import { RouterModule } from '@angular/router';
import { HanoiSceneModule } from '@components/hanoi/hanoi-scene/hanoi-scene.module';

@NgModule({
    declarations: [TowersOfHanoiComponent],
    imports: [CommonModule, RouterModule.forChild([{ path: '', component: TowersOfHanoiComponent }]), HanoiSceneModule],
})
export class TowersOfHanoiModule {}
