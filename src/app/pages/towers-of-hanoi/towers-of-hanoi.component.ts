import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-towers-of-hanoi',
    templateUrl: './towers-of-hanoi.component.html',
    styleUrls: ['./towers-of-hanoi.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TowersOfHanoiComponent implements OnInit {
    constructor(private readonly _progress: ProgressService) {}

    ngOnInit(): void {}

    complete() {
        this._progress.success();
    }
}
