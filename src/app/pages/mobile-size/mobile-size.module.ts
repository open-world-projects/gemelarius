import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileSizeComponent } from './mobile-size.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [MobileSizeComponent],
    imports: [CommonModule, RouterModule.forChild([{ path: '', component: MobileSizeComponent }])],
})
export class MobileSizeModule {}
