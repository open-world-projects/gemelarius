import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BehaviorSubject, filter, fromEvent, takeUntil, timer } from 'rxjs';
import { EmersionAnimation } from '@models/animation';
import { DestroyService } from '@services/destroy.service';
import { ProgressService } from '@services/progress.service';

@Component({
    selector: 'app-mobile-size',
    templateUrl: './mobile-size.component.html',
    styleUrls: ['./mobile-size.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
    animations: [EmersionAnimation],
})
export class MobileSizeComponent implements OnInit {
    isComplete$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    readonly EXPECTED_WIDTH = 540;
    constructor(private _destroy: DestroyService, private _progress: ProgressService) {}

    ngOnInit(): void {
        window.innerWidth <= this.EXPECTED_WIDTH && this.complete();
        fromEvent(window, 'resize')
            .pipe(takeUntil(this._destroy))
            .pipe(filter(() => !this.isComplete$.getValue()))
            .subscribe((e) => window.innerWidth <= this.EXPECTED_WIDTH && this.complete());
    }

    complete() {
        this.isComplete$.next(true);
        timer(1000).pipe(takeUntil(this._destroy)).subscribe(this._progress.success.bind(this._progress));
    }
}
