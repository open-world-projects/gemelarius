import { Inject, Injectable } from '@angular/core';
import { State, StateSaver, STATE_SAVER_TOKEN } from '@models/common/interfaces/StateSaver';
import { RouteApp } from '@models/common/RouteApp';
import { Utils } from '@Utils';

@Injectable({
    providedIn: 'root',
})
export class StateSaverService {
    constructor(@Inject(STATE_SAVER_TOKEN) private _stateStorage: StateSaver) {}

    saveProgress(path: string): void {
        const allValues = this._stateStorage.load();
        const parseValue = this.parseDataFromCrypto(allValues);
        const allKeys = Object.keys(RouteApp.app ?? {}).filter(
            (e: string) => (RouteApp?.app as unknown as any)?.[e]?.path
        );

        const vals = Object.values(RouteApp.app ?? {}).filter((e) => e?.path);
        const index = vals.findIndex((e) => e.short === path);
        index != allKeys.length - 1 &&
            index + 1 > parseValue.page &&
            this._stateStorage.save(this.dataToCrypto({ page: index + 1 }));
    }

    loadProgress(): State {
        const allValues = this._stateStorage.load();
        const parseValue = this.parseDataFromCrypto(allValues);
        return parseValue;
    }

    private parseDataFromCrypto(data: string | null): State {
        const state: State = { page: 0 };
        try {
            if (!data) {
                return state;
            }
            const parseString = String.fromCharCode(...data.split(':').map(Number));
            const parseState = JSON.parse(parseString) as State;
            state.page = parseState.page;
        } catch (error) {
            Utils.ShowError(error);
        } finally {
            return state;
        }
    }

    private dataToCrypto(obj: State): string {
        try {
            const str = JSON.stringify(obj);
            const crypto = str.split('').map((e, i) => str.charCodeAt(i));
            return crypto.join(':');
        } catch (error) {
            Utils.ShowError(error);
            const state: State = { page: 0 };
            const str = JSON.stringify(state);
            const crypto = str.split('').map((e, i) => str.charCodeAt(i));
            return crypto.join(':');
        }
    }
}
