import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ProgressService {
    public successStep: Subject<void> = new Subject<void>();
    constructor() {}

    success() {
        this.successStep.next();
    }
}
