import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrossingSceneComponent } from './crossing-scene.component';

@NgModule({
    declarations: [CrossingSceneComponent],
    imports: [CommonModule],
    exports: [CrossingSceneComponent],
})
export class CrossingSceneModule {}
