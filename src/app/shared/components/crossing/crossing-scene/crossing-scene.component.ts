import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { BoatAnimation } from '@models/animation';
import { CrossingDirection } from '@models/crossing';

@Component({
    selector: 'app-crossing-scene',
    templateUrl: './crossing-scene.component.html',
    styleUrls: ['./crossing-scene.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [BoatAnimation],
})
export class CrossingSceneComponent implements OnInit {
    @Input() dirBoat: CrossingDirection.LEFT | CrossingDirection.RIGHT = CrossingDirection.LEFT;

    @Output() endPathBoat: EventEmitter<void> = new EventEmitter<void>();

    @Output() clickDirection: EventEmitter<CrossingDirection> = new EventEmitter<CrossingDirection>();

    CrossingDirection = CrossingDirection;

    constructor() {}

    ngOnInit(): void {}
}
