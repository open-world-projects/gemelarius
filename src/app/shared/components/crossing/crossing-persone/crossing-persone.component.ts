import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { CrossingPersoneType } from '@models/crossing';

@Component({
    selector: 'app-crossing-persone',
    templateUrl: './crossing-persone.component.html',
    styleUrls: ['./crossing-persone.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CrossingPersoneComponent implements OnInit {
    @Input() type!: CrossingPersoneType;
    @Output() clickPersone: EventEmitter<void> = new EventEmitter<void>();

    CrossingPersoneType = CrossingPersoneType;
    constructor() {}

    ngOnInit(): void {}
}
