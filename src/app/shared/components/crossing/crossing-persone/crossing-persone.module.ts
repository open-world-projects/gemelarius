import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrossingPersoneComponent } from './crossing-persone.component';

@NgModule({
    declarations: [CrossingPersoneComponent],
    imports: [CommonModule],
    exports: [CrossingPersoneComponent],
})
export class CrossingPersoneModule {}
