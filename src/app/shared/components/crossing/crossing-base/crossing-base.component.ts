import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { EmersionAnimation } from '@models/animation';
import { CheckConflict, CrossingDirection, CrossingPersoneList, CrossingPersoneType } from '@models/crossing';
import { DestroyService } from '@services/destroy.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BehaviorSubject, takeUntil, timer } from 'rxjs';

@Component({
    selector: 'app-crossing-base',
    templateUrl: './crossing-base.component.html',
    styleUrls: ['./crossing-base.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
    animations: [EmersionAnimation],
})
export class CrossingBaseComponent implements OnInit {
    @Output() complete: EventEmitter<void> = new EventEmitter<void>();

    dirBoat: CrossingDirection.LEFT | CrossingDirection.RIGHT = CrossingDirection.LEFT;

    CrossingDirection = CrossingDirection;

    left$: BehaviorSubject<CrossingPersoneList[]> = new BehaviorSubject<CrossingPersoneList[]>([]);
    boat$: BehaviorSubject<CrossingPersoneList[]> = new BehaviorSubject<CrossingPersoneList[]>([]);
    right$: BehaviorSubject<CrossingPersoneList[]> = new BehaviorSubject<CrossingPersoneList[]>([]);

    isComplete$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private _destroy: DestroyService, private _msg: NzMessageService) {}

    ngOnInit(): void {
        this.left$.next([
            { type: CrossingPersoneType.DRIVER, conflict: [] },
            { type: CrossingPersoneType.CABBAGE, conflict: [CrossingPersoneType.GOAT] },
            { type: CrossingPersoneType.GOAT, conflict: [CrossingPersoneType.CABBAGE, CrossingPersoneType.WOLF] },
            { type: CrossingPersoneType.WOLF, conflict: [CrossingPersoneType.GOAT] },
        ]);
    }

    clickPersone(item: CrossingPersoneList, dir: CrossingDirection) {
        if (this.isComplete$.getValue()) {
            return;
        }
        if (dir === this.dirBoat) {
            const listBoat = this.boat$.getValue();
            const listDir = this[`${dir}$`].getValue();
            const selectedIndexDir = listDir.findIndex((e) => e.type === item.type);
            const selectedIndexBoat = listBoat.findIndex((e) => e.type === item.type);
            if (~selectedIndexDir) {
                // dir->boat
                if (listBoat.length < 2) {
                    const item = listDir[selectedIndexDir];
                    listDir.splice(selectedIndexDir, 1), listBoat.push(item);
                } else {
                    this._msg.warning(`Boat is full!`);
                }
            } else {
                // boat->dir
                const item = listBoat[selectedIndexBoat];
                listBoat.splice(selectedIndexBoat, 1), listDir.push(item);
            }
        }
        if (dir === CrossingDirection.BOAT) {
            const listDir = this[`${this.dirBoat}$`].getValue();
            const listBoat = this.boat$.getValue();
            const selectdIndexItem = listBoat.findIndex((e) => e.type === item.type);
            if (~selectdIndexItem) {
                const item = listBoat.splice(selectdIndexItem, 1);
                listDir.push(...item);
            }
        }
    }

    clickDirection(dir: CrossingDirection) {
        if (this.isComplete$.getValue()) {
            return;
        }
        if (dir !== this.dirBoat && dir !== CrossingDirection.BOAT) {
            if (this.boat$.getValue().find((e) => e.type === CrossingPersoneType.DRIVER)) {
                const conflict = CheckConflict(this[`${this.dirBoat}$`].getValue());
                if (!conflict) {
                    this.dirBoat = dir;
                } else {
                    this._msg.warning(`Conflict!`);
                }
            } else {
                this._msg.warning(`Boat without driver!`);
            }
        }
    }

    endPathBoat() {
        const listDir = this[`${this.dirBoat}$`].getValue();
        listDir.push(...this.boat$.getValue()), this.boat$.next([]);
        this.checkComplete();
    }

    private checkComplete() {
        const listLeft = this.left$.getValue();
        const listBoat = this.boat$.getValue();

        if (!listLeft.length && !listBoat.length) {
            timer(200)
                .pipe(takeUntil(this._destroy))
                .subscribe(() => this.isComplete$.next(true));

            timer(1000)
                .pipe(takeUntil(this._destroy))
                .subscribe(() => this.complete.next());
        }
    }
}
