import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrossingBaseComponent } from './crossing-base.component';
import { CrossingSceneModule } from '../crossing-scene/crossing-scene.module';
import { CrossingPersoneModule } from '../crossing-persone/crossing-persone.module';

@NgModule({
    declarations: [CrossingBaseComponent],
    imports: [CommonModule, CrossingSceneModule, CrossingPersoneModule],
    exports: [CrossingBaseComponent],
})
export class CrossingBaseModule {}
