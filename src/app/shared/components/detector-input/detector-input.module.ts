import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetectorInputComponent } from './detector-input.component';
import { FormsModule } from '@angular/forms';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzInputModule } from 'ng-zorro-antd/input';

@NgModule({
    declarations: [DetectorInputComponent],
    imports: [CommonModule, FormsModule, NzModalModule, NzInputModule],
    exports: [DetectorInputComponent],
})
export class DetectorInputModule {}
