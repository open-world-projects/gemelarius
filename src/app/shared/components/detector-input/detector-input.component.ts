import { ChangeDetectionStrategy, Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject, filter, takeUntil, timer } from 'rxjs';
import { DestroyService } from '@services/destroy.service';

@Component({
    selector: 'app-detector-input',
    templateUrl: './detector-input.component.html',
    styleUrls: ['./detector-input.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
})
export class DetectorInputComponent implements OnInit {
    @Input() expectedWord!: string;
    @Input() successWord: string = 'success';
    @Output() success: EventEmitter<void> = new EventEmitter<void>();

    isVisible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    word$: BehaviorSubject<string> = new BehaviorSubject<string>('');

    constructor(private _destroy: DestroyService) {}

    ngOnInit(): void {
        this.word$
            .pipe(takeUntil(this._destroy))
            .pipe(filter(Boolean))
            .pipe(filter((e) => !this.isVisible$.getValue()))
            .subscribe(this.show.bind(this));

        this.word$
            .pipe(takeUntil(this._destroy))
            .pipe(filter(Boolean))
            .pipe(filter((e) => e === this.expectedWord && e !== this.successWord))
            .subscribe(this.successEvent.bind(this));
    }

    @HostListener('window:keydown', ['$event'])
    public keyDown(event: KeyboardEvent) {
        if (
            (event.type === 'keydown' && event?.keyCode >= 48 && event?.keyCode <= 57) ||
            (event?.keyCode >= 65 && event?.keyCode <= 90)
        ) {
            this.word$.next(`${this.word$.getValue()}${event.key}`);
        }
        // TODO: add timer to hide modal
    }

    hide() {
        this.isVisible$.next(false);
        this.word$.next('');
    }

    show() {
        this.isVisible$.next(true);
    }

    successEvent() {
        timer(10)
            .pipe(takeUntil(this._destroy))
            .subscribe(() => this.word$.next(this.successWord));
        timer(500)
            .pipe(takeUntil(this._destroy))
            .subscribe(() => (this.success.emit(), this.hide()));
    }
}
