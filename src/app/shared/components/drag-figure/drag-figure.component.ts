import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Input, OnInit } from '@angular/core';
import { Utils } from '@Utils';
import { FigureAxisType, FigureType } from './FigureType';

@Component({
    selector: 'app-drag-figure',
    templateUrl: './drag-figure.component.html',
    styleUrls: ['./drag-figure.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DragFigureComponent implements OnInit {
    @Input() type: FigureType = FigureType.SQUARE;
    @Input() dragBoundary: string | ElementRef<HTMLElement> | HTMLElement = '';
    @Input() dragLockAxis: FigureAxisType = FigureAxisType.X;

    @Input() initX: number = 0;
    @Input() initY: number = 0;

    private guid = Utils.Uuidv4();

    @HostBinding('class')
    public get getClass() {
        return this.type;
    }

    @HostBinding('class')
    public get getClassGuid() {
        return `guid-${this.guid}`;
    }

    @HostBinding('style.left.px')
    public get getLeft() {
        return this.initX;
    }

    @HostBinding('style.top.px')
    public get getTop() {
        return this.initY;
    }

    constructor() {}

    ngOnInit(): void {}
}
