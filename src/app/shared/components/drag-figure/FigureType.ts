export enum FigureType {
    CIRCLE = 'circle',
    SQUARE = 'square',
}

export enum FigureAxisType {
    X = 'x',
    Y = 'y',
}
