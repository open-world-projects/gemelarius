import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragFigureComponent } from './drag-figure.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
@NgModule({
    declarations: [DragFigureComponent],
    imports: [CommonModule, DragDropModule],
    exports: [DragFigureComponent],
})
export class DragFigureModule {}
