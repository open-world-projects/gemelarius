import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, HostBinding } from '@angular/core';

@Component({
    selector: 'app-bubble',
    templateUrl: './bubble.component.html',
    styleUrls: ['./bubble.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BubbleComponent implements OnInit {
    @Input() ordinal!: number;

    @Input() initX!: number;
    @Input() initY!: number;

    @Output() clicked: EventEmitter<void> = new EventEmitter<void>();
    constructor() {}

    @HostBinding('style.left.px')
    public get getLeft() {
        return this.initX;
    }
    @HostBinding('style.top.px')
    public get getTop() {
        return this.initY;
    }

    ngOnInit(): void {}
}
