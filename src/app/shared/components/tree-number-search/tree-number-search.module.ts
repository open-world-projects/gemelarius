import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeNumberSearchComponent } from './tree-number-search.component';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
    declarations: [TreeNumberSearchComponent],
    imports: [CommonModule, NzIconModule],
    exports: [TreeNumberSearchComponent],
})
export class TreeNumberSearchModule {}
