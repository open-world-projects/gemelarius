import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EmersionAnimation, TreeLeafAnimation } from '@models/animation';
import { BinaryTree, Node } from '@models/BinaryTree';
import { Utils } from '@Utils';

@Component({
    selector: 'app-tree-number-search',
    templateUrl: './tree-number-search.component.html',
    styleUrls: ['./tree-number-search.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [EmersionAnimation, TreeLeafAnimation],
})
export class TreeNumberSearchComponent implements OnInit {
    @Input() expectedNumber!: number;

    @Input() countValueInTree: number = 25;

    @Output() complete: EventEmitter<void> = new EventEmitter<void>();

    isComplete$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    // controls
    // =============================================================
    isAccessBack$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    leafs$: BehaviorSubject<(Node<number> | null)[]> = new BehaviorSubject<(Node<number> | null)[]>([]);
    // =============================================================

    private _tree: BinaryTree<number> = new BinaryTree<number>((a, b) => a < b);

    constructor() {}

    ngOnInit(): void {
        const mas: number[] = [];
        while (mas.length < this.countValueInTree) {
            const rndV = Utils.RandomValueFromRange(this.expectedNumber - 20, this.expectedNumber + 20);
            rndV !== 0 && this.expectedNumber !== rndV && !mas.includes(rndV) && mas.push(rndV);
        }
        mas.forEach((e) => this._tree.insertValue(e));
        this._tree.insertValue(this.expectedNumber);
        this.leafs$.next([this._tree.getRoot()]);
    }

    // logic clicks

    clickLeaf(leaf: Node<number> | null): void {
        if (!leaf) {
            return;
        }
        if (leaf.value === this.expectedNumber) {
            this.isComplete$.next(true), this.complete.emit();
        } else {
            this.setChildren(leaf);
        }
    }

    setChildren(node: Node<number> | null) {
        const values: (Node<number> | null)[] = [];
        values.push(node?.left ?? null);
        values.push(node?.right ?? null);
        if (!values.filter(Boolean).length) {
            return;
        }
        this.leafs$.next(values);
        this.isAccessBack$.next(true);
    }

    back(): void {
        const elem = this.leafs$.getValue().filter(Boolean).pop();
        if (elem?.parent?.parent) {
            this.setChildren(elem?.parent?.parent);
        } else {
            this.leafs$.next([this._tree.getRoot()]);
            this.isAccessBack$.next(false);
        }
    }
}
