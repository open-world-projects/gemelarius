import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { CircleAnimation } from '@models/animation';

@Component({
    selector: 'app-cycle-number-circle',
    templateUrl: './cycle-number-circle.component.html',
    styleUrls: ['./cycle-number-circle.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [CircleAnimation(9)],
})
export class CycleNumberCircleComponent implements OnInit {
    @Input() segments: number = 9;

    arraySegments: number[] = [];
    closeSegments: boolean[] = [];

    stepAnimation = 's-0';

    constructor() {}

    ngOnInit(): void {
        this.arraySegments = Array.from({ length: this.segments }).map((_, i) => this.closeSegments.push(i % 3 !== 0));
    }

    nextStep(): void {
        const split = +this.stepAnimation.split('-')[1];
        this.stepAnimation = 's-' + (split === this.segments ? 1 : (split + 1) % (this.segments + 1));
    }

    reset(): void {
        this.stepAnimation = 's-0';
    }
}
