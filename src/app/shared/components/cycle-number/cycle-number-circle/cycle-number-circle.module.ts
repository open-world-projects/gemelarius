import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CycleNumberCircleComponent } from './cycle-number-circle.component';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
    declarations: [CycleNumberCircleComponent],
    imports: [CommonModule, NzIconModule],
    exports: [CycleNumberCircleComponent],
})
export class CycleNumberCircleModule {}
