import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CycleNumberSceneComponent } from './cycle-number-scene.component';
import { CycleNumberCircleModule } from '../cycle-number-circle/cycle-number-circle.module';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [CycleNumberSceneComponent],
    imports: [CommonModule, CycleNumberCircleModule, NzIconModule, NzInputNumberModule, FormsModule],
    exports: [CycleNumberSceneComponent],
})
export class CycleNumberSceneModule {}
