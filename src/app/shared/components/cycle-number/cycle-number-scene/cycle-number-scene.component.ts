import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { EmersionAnimation } from '@models/animation';
import { DestroyService } from '@services/destroy.service';
import { Utils } from '@Utils';
import { BehaviorSubject, takeUntil, timer } from 'rxjs';

@Component({
    selector: 'app-cycle-number-scene',
    templateUrl: './cycle-number-scene.component.html',
    styleUrls: ['./cycle-number-scene.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
    animations: [EmersionAnimation],
})
export class CycleNumberSceneComponent implements OnInit {
    readonly COUNT = 5;

    settings: { rotateNumber: number[]; rotateBoolean: boolean[]; models: { value: number }[] } = {
        rotateNumber: [],
        rotateBoolean: [],
        models: [],
    };

    complete: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    showSuccess: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    @Output() success: EventEmitter<void> = new EventEmitter<void>();

    private _answer: number[] = [];

    constructor(private readonly destroy$: DestroyService) {}

    ngOnInit(): void {
        Array.from({ length: this.COUNT }).map(
            () => (
                this.settings.rotateBoolean.push(Utils.RandomBooleanValue()),
                this.settings.models.push({ value: 1 }),
                this.settings.rotateNumber.push(Utils.RandomValueFromRange(1, 9))
            )
        );
        this._answer = this.generateAnswer();
        this.modelChange();
    }

    modelChange() {
        if (this.complete.getValue()) {
            return;
        }
        const finish = this._answer.every((e, i) => e === this.settings.models[i].value);
        finish && this.finish();
    }

    private finish() {
        this.complete.next(true);
        timer(200)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.showSuccess.next(true));
        timer(1200)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.success.emit());
    }

    private generateAnswer(init: number[] = [1, 4, 7]) {
        const rotate = (mas: number[]) => mas.map((e) => (e === 9 ? 1 : e + 1));
        const answer = [];
        for (let i = 0; i < this.settings.rotateNumber.length; i++) {
            const temp: number[] = Array.from({ length: this.settings.rotateNumber[i] }).reduce(
                (prev: number[]) => rotate(prev),
                [...init]
            );
            answer.push(Math[this.settings.rotateBoolean[i] ? 'max' : 'min'](...temp));
        }
        return answer;
    }
}
