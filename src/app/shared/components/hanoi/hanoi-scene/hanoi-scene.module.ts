import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HanoiSceneComponent } from './hanoi-scene.component';
import { HanoiRingModule } from '../hanoi-ring/hanoi-ring.module';
import { HanoiTowerModule } from '../hanoi-tower/hanoi-tower.module';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
    declarations: [HanoiSceneComponent],
    imports: [CommonModule, HanoiTowerModule, HanoiRingModule, DragDropModule],
    exports: [HanoiSceneComponent],
})
export class HanoiSceneModule {}
