import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { EmersionAnimation } from '@models/animation';
import { TowerList } from '@models/hanoi';
import { DestroyService } from '@services/destroy.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BehaviorSubject, takeUntil, timer } from 'rxjs';

@Component({
    selector: 'app-hanoi-scene',
    templateUrl: './hanoi-scene.component.html',
    styleUrls: ['./hanoi-scene.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [DestroyService],
    animations: [EmersionAnimation],
})
export class HanoiSceneComponent implements OnInit {
    @Input() towers: number = 3;

    @Output() complete: EventEmitter<void> = new EventEmitter<void>();

    towers$: BehaviorSubject<TowerList[]> = new BehaviorSubject<TowerList[]>([]);
    isComplete$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    idTowers$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

    constructor(private _msg: NzMessageService, private _destroy: DestroyService) {}

    ngOnInit(): void {
        const list: TowerList[] = Array.from({ length: this.towers }).map((e) => ({ val: [] }));
        list[0].val = [7, 6, 5, 4, 3, 2, 1];
        this.idTowers$.next(Array.from({ length: this.towers }).map((e, i) => `t-${i}`));
        this.towers$.next(list);
    }

    dropped(event: CdkDragDrop<any, any>) {
        if (this.isComplete$.getValue()) {
            return;
        }

        const list: number[] = event.container.data;
        const size: number = event.item.data;

        // this is another tower
        if (!list.includes(size)) {
            const to: string = event.container.id;
            const idxTo = this.idTowers$.getValue().indexOf(to);
            if (~idxTo) {
                if ((list?.[0] ?? 0) < size) {
                    const items = event.previousContainer.data.splice(0, 1);
                    list.unshift(...items);
                } else {
                    this._msg.warning('You can only move to smaller rings!');
                }
            }
        } else {
        }
        this.check();
    }

    check() {
        const fullTower = this.towers$.getValue()?.some((e, i) => i !== 0 && e.val.length === 7);
        fullTower && this.success();
    }

    success() {
        timer(100)
            .pipe(takeUntil(this._destroy))
            .subscribe(() => this.isComplete$.next(true));

        timer(1000)
            .pipe(takeUntil(this._destroy))
            .subscribe(() => this.complete.emit());
    }
}
