import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HanoiTowerComponent } from './hanoi-tower.component';

@NgModule({
    declarations: [HanoiTowerComponent],
    imports: [CommonModule],
    exports: [HanoiTowerComponent],
})
export class HanoiTowerModule {}
