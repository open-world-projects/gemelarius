import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-hanoi-tower',
  templateUrl: './hanoi-tower.component.html',
  styleUrls: ['./hanoi-tower.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HanoiTowerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
