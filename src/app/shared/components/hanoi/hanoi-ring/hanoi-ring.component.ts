import { Component, OnInit, ChangeDetectionStrategy, Input, HostBinding } from '@angular/core';
import { RingSize } from '@models/hanoi';

@Component({
    selector: 'app-hanoi-ring',
    templateUrl: './hanoi-ring.component.html',
    styleUrls: ['./hanoi-ring.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HanoiRingComponent implements OnInit {
    @Input() size: RingSize = 1;

    constructor() {}

    @HostBinding('class')
    public get sizeClass() {
        return `s-${this.size}`;
    }

    ngOnInit(): void {}
}
