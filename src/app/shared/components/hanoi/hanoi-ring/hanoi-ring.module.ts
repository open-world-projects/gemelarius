import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HanoiRingComponent } from './hanoi-ring.component';

@NgModule({
    declarations: [HanoiRingComponent],
    imports: [CommonModule],
    exports: [HanoiRingComponent],
})
export class HanoiRingModule {}
