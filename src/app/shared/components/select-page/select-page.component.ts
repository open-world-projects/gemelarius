import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { RouteTypeList } from '@models/RouteTypeList';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-select-page',
    templateUrl: './select-page.component.html',
    styleUrls: ['./select-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectPageComponent implements OnInit {
    @Input() pages: RouteTypeList[] = [];

    @Output() selectPage: EventEmitter<RouteTypeList> = new EventEmitter<RouteTypeList>();

    isVisible$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() {}

    ngOnInit(): void {}

    openPage(item: RouteTypeList) {
        if (item.disabled) {
            return;
        }
        this.hide();
        this.selectPage.emit(item);
    }

    show() {
        this.changeVisible(true);
    }

    hide() {
        this.changeVisible(false);
    }

    afterHide() {}

    private changeVisible(isVisible: boolean): void {
        this.isVisible$.next(isVisible);
    }
}
