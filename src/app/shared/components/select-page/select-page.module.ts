import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectPageComponent } from './select-page.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
    declarations: [SelectPageComponent],
    imports: [CommonModule, NzModalModule, NzIconModule],
    exports: [SelectPageComponent],
})
export class SelectPageModule {}
