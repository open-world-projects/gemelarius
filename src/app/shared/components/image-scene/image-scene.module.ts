import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageSceneComponent } from './image-scene.component';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [ImageSceneComponent],
    imports: [CommonModule, NzSliderModule, FormsModule],
    exports: [ImageSceneComponent],
})
export class ImageSceneModule {}
