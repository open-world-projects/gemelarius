import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
    selector: 'app-image-scene',
    templateUrl: './image-scene.component.html',
    styleUrls: ['./image-scene.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageSceneComponent implements OnInit {
    @Input() word: string = '';

    controlGreen: number = 20;
    controlRed: number = 50;
    controlBlue: number = 200;

    constructor() {}

    ngOnInit(): void {}
}
