import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, takeUntil, timer } from 'rxjs';
import { EmersionAnimation } from '@models/animation';
import { DestroyService } from '@services/destroy.service';

interface ListCubes {
    value: number;
}

@Component({
    selector: 'app-trifoliate-cubes-base',
    templateUrl: './trifoliate-cubes-base.component.html',
    styleUrls: ['./trifoliate-cubes-base.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [EmersionAnimation],
    providers: [DestroyService],
})
export class TrifoliateCubesBaseComponent implements OnInit {
    @Input() cubes: number = 4;
    @Output() complete: EventEmitter<void> = new EventEmitter<void>();

    list$: BehaviorSubject<ListCubes[]> = new BehaviorSubject<ListCubes[]>([]);
    isComplete$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private _destroy: DestroyService) {}

    ngOnInit(): void {
        const initialMas: ListCubes[] = Array.from({ length: this.cubes }).map((e, i) => ({
            value: (i % 3) + 1,
        }));
        const equals = initialMas.every((e) => e === initialMas[0]);
        equals && (initialMas[initialMas.length - 1].value = (initialMas[initialMas.length - 1].value % 3) + 1);
        this.list$.next(initialMas);
    }

    click(index: number): void {
        if (this.isComplete$.getValue()) {
            return;
        }
        const list = this.list$.getValue();
        list[index].value = (list[index].value % 3) + 1;
        index > 0 && (list[index - 1].value = (list[index - 1].value % 3) + 1);
        index < list.length - 1 && (list[index + 1].value = (list[index + 1].value % 3) + 1);
        this.check();
    }

    private check() {
        const list = this.list$.getValue();
        const value = list[0];
        list.every((e) => e.value === value.value) && this.success();
    }

    private success() {
        timer(100)
            .pipe(takeUntil(this._destroy))
            .subscribe((e) => this.isComplete$.next(true));
        timer(1100)
            .pipe(takeUntil(this._destroy))
            .subscribe((e) => this.complete.emit());
    }
}
