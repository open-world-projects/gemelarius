import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrifoliateCubesBaseComponent } from './trifoliate-cubes-base.component';
import { TrifoliateCubeModule } from '../trifoliate-cube/trifoliate-cube.module';

@NgModule({
    declarations: [TrifoliateCubesBaseComponent],
    imports: [CommonModule, TrifoliateCubeModule],
    exports: [TrifoliateCubesBaseComponent],
})
export class TrifoliateCubesBaseModule {}
