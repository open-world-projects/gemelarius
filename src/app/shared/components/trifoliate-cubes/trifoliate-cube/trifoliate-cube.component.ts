import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
    selector: 'app-trifoliate-cube',
    templateUrl: './trifoliate-cube.component.html',
    styleUrls: ['./trifoliate-cube.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrifoliateCubeComponent implements OnInit {
    @Input() leafCounter: number = 1;

    constructor() {}

    ngOnInit(): void {}
}
