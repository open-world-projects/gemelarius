import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrifoliateCubeComponent } from './trifoliate-cube.component';

@NgModule({
    declarations: [TrifoliateCubeComponent],
    imports: [CommonModule],
    exports: [TrifoliateCubeComponent],
})
export class TrifoliateCubeModule {}
