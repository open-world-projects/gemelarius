export class Utils {
    static ShowError(error: unknown, callable?: Function): void {
        console.error(error);
        callable?.(error);
    }

    static Uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == 'x' ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });
    }

    static RandomValueFromObject<T>(obj: any): T {
        const keys = Object.keys(obj);

        return obj[keys[Math.floor(Math.random() * keys.length)]] as T;
    }

    static RandomKeyFromObject(obj: any): string {
        const keys = Object.keys(obj);
        const rndIndex = this.RandomValueFromRange(0, keys.length);
        return keys[rndIndex];
    }

    static RandomKeyInEnum(anEnum: any): string {
        const enumValues = Object.keys(anEnum)
            .map((n) => Number.parseInt(n))
            .filter((n) => !Number.isNaN(n));
        const randomIndex = this.RandomValueFromRange(0, enumValues.length);
        const randomEnumValue = enumValues[randomIndex];
        return anEnum[randomEnumValue];
    }

    static DiffDateInDays(first: Date, second: Date): number {
        const start = Math.floor(first.getTime() / (3600 * 24 * 1000));
        const end = Math.floor(second.getTime() / (3600 * 24 * 1000));
        const daysDiff = Math.abs(end - start);
        return daysDiff;
    }

    static RandomValueFromRange(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min) + min);
    }

    static RandomBooleanValue(): boolean {
        return Math.random() > 0.5;
    }

    static Shuffle<T = any>(array: T[]) {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    static copyObject<T = any>(obj: T): T {
        return JSON.parse(JSON.stringify(obj));
    }
}
