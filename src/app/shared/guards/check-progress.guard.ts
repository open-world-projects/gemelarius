import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { RouteApp, RouteType } from '@models/common/RouteApp';
import { StateSaverService } from '@services/state-saver.service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class CheckProgressGuard implements CanActivateChild {
    constructor(private readonly _state: StateSaverService, private readonly _route: Router) {}
    canActivateChild(
        childRoute: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        const data = childRoute.url[0];
        if (data && environment.production) {
            const lastState = this._state.loadProgress();
            const idx = Object.values(RouteApp.app ?? {})
                .filter((e) => e?.path)
                .findIndex((e: RouteType) => e.short === data.path);
            if (lastState.page < idx) {
                this._route.navigate([Object.values(RouteApp.app ?? {}).filter((e) => e?.path)[lastState.page]?.path]);
                return false;
            }
            return true;
        }
        return true;
    }
}
