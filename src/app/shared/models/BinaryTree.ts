export interface Node<T> {
    value: T;
    left?: Node<T>;
    right?: Node<T>;
    parent?: Node<T>;
}
export class BinaryTree<T = any> {
    root: Node<T> | null = null;

    constructor(private compareValue: (a: T, b: T) => boolean) {}

    insertValue(value: T): void {
        if (!this.root) {
            this.root = { value };
        } else {
            const node = this.root;
            const searchTree = (search: Node<T>) => {
                if (this.compareValue(value, search.value) && search.left) {
                    searchTree(search.left);
                } else if (this.compareValue(value, search.value)) {
                    search.left = { value, parent: search };
                } else if (!this.compareValue(value, search.value) && search.right) {
                    searchTree(search.right);
                } else if (!this.compareValue(value, search.value)) {
                    search.right = { value, parent: search };
                }
            };
            return searchTree(node);
        }
    }

    find(value: T): Readonly<Node<T> | null> {
        if (!this.root) {
            return null;
        }
        let node = this.root;
        let found: Node<T> | null = null;
        while (node && !found) {
            if (this.compareValue(value, node.value)) {
                node = node?.left as Node<T>;
            } else if (this.compareValue(node.value, value)) {
                node = node?.right as Node<T>;
            } else {
                found = node;
            }
        }

        return found;
    }

    getRoot(): Readonly<Node<T> | null> {
        return this.root;
    }
}
