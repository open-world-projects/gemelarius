export type RingSize = 1 | 2 | 3 | 4 | 5 | 6 | 7;

export interface TowerList {
    val: RingSize[];
}
