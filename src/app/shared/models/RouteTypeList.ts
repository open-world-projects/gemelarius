import { RouteType } from './common/RouteApp';

export interface RouteTypeList extends RouteType {
    disabled?: boolean;
    selected?: boolean;
}
