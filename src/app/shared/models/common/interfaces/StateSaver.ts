import { InjectionToken } from '@angular/core';

export interface StateSaver {
    save: (data: string | null) => boolean;
    load: () => string | null;
    delete: () => boolean;
}
export interface State {
    page: number;
}

export const STATE_SAVER_TOKEN = new InjectionToken<StateSaver>('State saver implementstion');
