import { Utils } from '@Utils';
import { StateSaver } from './interfaces/StateSaver';

export class StateSaverLocalStorage implements StateSaver {
    private readonly NAME_LOCAL_STORAGE = 'state';

    save(data: string | null) {
        try {
            localStorage.setItem(this.NAME_LOCAL_STORAGE, data ?? '');
        } catch (error) {
            Utils.ShowError(error);
            return false;
        }
        return true;
    }

    load(): string | null {
        try {
            return localStorage.getItem(this.NAME_LOCAL_STORAGE);
        } catch (error) {
            Utils.ShowError(error);
            return null;
        }
    }

    delete(): boolean {
        try {
            localStorage.removeItem(this.NAME_LOCAL_STORAGE);
            return true;
        } catch (error) {
            Utils.ShowError(error);
            return false;
        }
    }
}
