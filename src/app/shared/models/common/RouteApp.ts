import { InjectionToken } from '@angular/core';

export interface RouteType {
    short: string;
    path: string;
    name?: string;
    icon: string;
}

export interface RoutePaths {
    app?: {
        drag?: RouteType;
        changeDate?: RouteType;
        bubbleNumber?: RouteType;
        mobileSize?: RouteType;
        numberTree?: RouteType;
        imageProjection?: RouteType;
        trifoliateCubes?: RouteType;
        crossing?: RouteType;
        hanoi?: RouteType;
        findPare?: RouteType;
        gamePuzzle?: RouteType;
        cycleNumber?: RouteType;
        sudoku?: RouteType;
        resort?: RouteType;
        points?: RouteType;
        // TODO: more pages later

        toBeContinue?: RouteType;
    } & RouteType;
}

const combineRoutePath = (parentPath: string, shortCurrent: string, name: string, icon: string): RouteType => ({
    path: `${parentPath}/${shortCurrent}`,
    short: shortCurrent,
    name,
    icon,
});

const routeApp: RoutePaths = {};

routeApp.app = combineRoutePath('', 'app', 'Main', 'home');

routeApp.app.drag = combineRoutePath(routeApp.app.path, 'drag', 'Drag', 'drag');
routeApp.app.changeDate = combineRoutePath(routeApp.app.path, 'change-date', 'Change date', 'clock-circle');
routeApp.app.bubbleNumber = combineRoutePath(routeApp.app.path, 'bubble-number', 'Bubble number', 'aim');
routeApp.app.imageProjection = combineRoutePath(routeApp.app.path, 'image-projection', 'Image projection', 'picture');
routeApp.app.mobileSize = combineRoutePath(routeApp.app.path, 'mobile-size', 'Mobile size', 'swap');
routeApp.app.numberTree = combineRoutePath(routeApp.app.path, 'number-tree', 'Number tree', 'apartment');
routeApp.app.trifoliateCubes = combineRoutePath(routeApp.app.path, 'trifoliate-cubes', 'Trifoliate cubes', 'appstore');
routeApp.app.crossing = combineRoutePath(routeApp.app.path, 'crossing', 'Crossing', 'swap');
routeApp.app.hanoi = combineRoutePath(routeApp.app.path, 'hanoi', 'Tower of hanoi', 'sliders');
routeApp.app.findPare = combineRoutePath(routeApp.app.path, 'find-pare', 'Find pare', 'robot');
routeApp.app.gamePuzzle = combineRoutePath(routeApp.app.path, 'puzzle', 'Puzzle', 'appstore-add');
routeApp.app.cycleNumber = combineRoutePath(routeApp.app.path, 'cycle-number', 'Cycle number', 'sync');
routeApp.app.sudoku = combineRoutePath(routeApp.app.path, 'sudoku', 'Sudoku', 'borderless-table');
routeApp.app.resort = combineRoutePath(routeApp.app.path, 'resort', 'Resort', 'sort-descending');
routeApp.app.points = combineRoutePath(routeApp.app.path, 'points', 'Points', 'branches');
routeApp.app.toBeContinue = combineRoutePath(routeApp.app.path, 'to-be-continue', 'To be continue', 'dash');

export const RouteApp: RoutePaths = Object.freeze(routeApp);

export const ROUTE_APP_TOKEN = new InjectionToken<RoutePaths>('Route paths implementation');
