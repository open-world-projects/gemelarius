import {
    animate,
    animateChild,
    group,
    keyframes,
    query,
    stagger,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { RouteApp } from './common/RouteApp';

const keyPath = Object.values(RouteApp?.app ?? {})
    .filter((e) => e?.path)
    .map((e) => e.short);

const keyPathFunc = (path: string[]) =>
    path
        .map((e, i) =>
            path
                .slice(i + 1)
                .map((t) => `${e} => ${t}`)
                .filter(Boolean)
                .join(', ')
        )
        .filter(Boolean)
        .join(', ');

export const routeTransitionAnimations = trigger('triggerName', [
    transition(keyPathFunc(keyPath), [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                right: 0,
                width: '100%',
            }),
        ]),
        query(':enter', [style({ right: '-100%', opacity: 0 })]),
        query(':leave', animateChild()),
        group([
            query(':leave', [animate('1s ease-out', style({ right: '100%', opacity: 0 }))]),
            query(':enter', [animate('1s ease-out', style({ right: '0%', opacity: 1 }))]),
        ]),
        query(':enter', animateChild()),
    ]),
    transition(keyPathFunc(keyPath.reverse()), [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
            }),
        ]),
        query(':enter', [style({ left: '-100%', opacity: 0 })]),
        query(':leave', animateChild()),
        group([
            query(':leave', [animate('1s ease-out', style({ left: '100%', opacity: 0 }))]),
            query(':enter', [animate('1s ease-out', style({ left: '0%', opacity: 1 }))]),
        ]),
        query(':enter', animateChild()),
    ]),
]);

export const BubbleAnimation = trigger('bubble', [
    transition('* => *', [
        query(
            ':enter',
            [
                style({ transform: 'scale(0)', opacity: 0 }),
                stagger(100, [animate('0.1s', style({ transform: 'scale(1)', opacity: 1 }))]),
            ],
            {
                optional: true,
            }
        ),
        query(
            ':leave',
            [
                style({ transform: 'scale(1)', opacity: 1 }),
                stagger(100, [animate('0.1s', style({ transform: 'scale(0)', opacity: 0 }))]),
            ],
            {
                optional: true,
            }
        ),
    ]),
]);

export const EmersionAnimation = trigger('emersion', [
    transition(':enter', [
        style({ transform: 'scale(0)', opacity: 0 }),
        animate('0.2s', style({ transform: 'scale(1)', opacity: 1 })),
    ]),
    transition(':leave', [
        style({ transform: 'scale(1)', opacity: 1 }),
        animate('0.2s', style({ transform: 'scale(0)', opacity: 0 })),
    ]),
]);

export const TreeLeafAnimation = trigger('leaf', [
    transition(':enter', [
        style({ transform: 'translateX(-100%)', opacity: 0 }),
        animate('0.2s 0.3s', style({ transform: 'translateX(0)', opacity: 1 })),
    ]),
    transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1 }),
        animate('0.2s', style({ transform: 'translateX(-100%)', opacity: 0 })),
    ]),
]);

export const BoatAnimation = trigger('boat', [
    state('left', style({ left: '0' })),
    state('right', style({ left: '50%' })),
    transition('* => *', [animate('0.5s')]),
]);

export const CircleAnimation = (count: number) =>
    trigger('rotate', [
        ...Array.from({ length: count + 1 }).map((_, i) =>
            state(`s-${i}`, style({ transform: `rotate(${(360 / count) * i}deg)` }))
        ),
        transition(`s-${count} => s-1`, [
            animate(
                '0.5s ease',
                keyframes([
                    style({ transform: `rotate(360deg)`, offset: 0 }),
                    style({ transform: `rotate(0deg)`, offset: 0.001 }),
                    style({ transform: `rotate(${360 / count}deg)`, offset: 1 }),
                ])
            ),
        ]),
        transition('* => *', [animate('0.5s ease')]),
    ]);

export const PuzzleShift = (size: number) =>
    trigger('shift', [
        ...Array.from({ length: size })
            .map((_, i) =>
                Array.from({ length: size }).map((__, j) =>
                    state(`pos-${i + 1}-${j + 1}`, style({ left: `${(100 / size) * j}%`, top: `${(100 / size) * i}%` }))
                )
            )
            .flat(),

        transition('* => *', [animate('0.2s ease')]),
    ]);
