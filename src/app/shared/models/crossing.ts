export enum CrossingPersoneType {
    DRIVER = 'Driver',
    WOLF = 'Wolf',
    GOAT = 'Goat',
    CABBAGE = 'Gabbage',
}

export enum CrossingDirection {
    LEFT = 'left',
    BOAT = 'boat',
    RIGHT = 'right',
}

export interface CrossingPersoneList {
    type: CrossingPersoneType;
    conflict: CrossingPersoneType[];
}

export const CheckConflict = (list: CrossingPersoneList[]): boolean => {
    let conflict = false;

    for (let i = 0; i < list.length; i++) {
        const sliceList = list.filter((_, index) => i !== index).map((e) => e.type);
        if (list[i].conflict.some((e) => sliceList.includes(e))) {
            conflict = true;
        }
    }

    return conflict;
};
