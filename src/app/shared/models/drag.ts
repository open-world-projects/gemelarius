import { FigureType, FigureAxisType } from '@components/drag-figure/FigureType';

export enum DragWords {
    GAME,
    SPARK,
    MAX,
    ENDING,
    OPEN,
    CLOSE,
    SEVEN,
    FOCUS,
}

export interface ListFigure {
    type: FigureType;
    axis: FigureAxisType;
    x: number;
    y: number;
}

export interface ListWord {
    word: string;
    x: number;
    y: number;
}
