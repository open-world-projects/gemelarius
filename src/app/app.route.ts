import { Route } from '@angular/router';
import { RouteApp } from '@models/common/RouteApp';

export const APP_ROUTES: Route[] = [
    {
        path: RouteApp.app?.short,
        loadChildren: () => import('./pages/main/main.module').then((e) => e.MainModule),
    },
    {
        path: '**',
        redirectTo: RouteApp.app?.short,
    },
];
